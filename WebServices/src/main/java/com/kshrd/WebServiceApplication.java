package com.kshrd;


import com.kshrd.models.*;
import com.kshrd.models.from.FrmsocailMedia;
import com.kshrd.repositories.*;
import com.kshrd.services.GroupService;
import com.kshrd.services.UserGroupService;
import com.kshrd.services.UserService;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.sql.Timestamp;


@SpringBootApplication


public class WebServiceApplication extends SpringBootServletInitializer {

    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {

        return builder.sources(WebServiceApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(WebServiceApplication.class, args);
    }


}
