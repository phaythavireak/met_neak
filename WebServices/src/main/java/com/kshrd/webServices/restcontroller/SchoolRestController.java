package com.kshrd.webServices.restcontroller;


import com.kshrd.models.School;
import com.kshrd.services.SchoolService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/school")
public class SchoolRestController {

    private SchoolService schoolService;

    public SchoolRestController(SchoolService schoolService) {
        this.schoolService = schoolService;
    }


    @GetMapping(value = "/getall",produces = {"application/json"})
    public Map<String,Object> getall()
    {
        Map<String,Object> map = new HashMap<>();
        List<School> schools = this.schoolService.getall();

        if(!schools.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",schools);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }
    @GetMapping(value = "/getallHighschool",produces = {"application/json"})
    public Map<String,Object> getallHighschool()
    {
        Map<String,Object> map = new HashMap<>();
        List<School> Highschool = this.schoolService.getAllHighSchools();

        if(!Highschool.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",Highschool);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }
    @GetMapping(value = "/getallSeconaryschool",produces = {"application/json"})
    public Map<String,Object> getallSeconaryschool()
    {
        Map<String,Object> map = new HashMap<>();
        List<School> Highschool = this.schoolService.getAllSecondaySchools();

        if(!Highschool.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",Highschool);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }
    @GetMapping(value = "/getallprimaryschool",produces = {"application/json"})
    public Map<String,Object> getallprimaryschool()
    {
        Map<String,Object> map = new HashMap<>();
        List<School> Highschool = this.schoolService.getAllPrimarySchools();

        if(!Highschool.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",Highschool);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }



}
