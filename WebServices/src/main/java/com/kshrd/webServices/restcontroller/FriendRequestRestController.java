package com.kshrd.webServices.restcontroller;
import com.kshrd.models.FriendRequest;
import com.kshrd.services.FriendRequestService;
import com.kshrd.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/friendRequest")
public class FriendRequestRestController {
    private HttpStatus httpStatus ;
    private Map<String,Object> map;

    @Autowired
    public FriendRequestRestController(FriendRequestService friendRequestService, NotificationService notificationService) {
        this.friendRequestService = friendRequestService;
        this.notificationService = notificationService;
    }

    private FriendRequestService friendRequestService;
    private NotificationService notificationService;
    //Save FriendRequest
    @PostMapping(value = "/{sendId}/{receiverId}")
    public ResponseEntity<Map<String,Object>> saveFriendRequest(@PathVariable("sendId") int sendId,@PathVariable("receiverId") int receiverId)
    {
        map=new HashMap<>();
        try {
            if (friendRequestService.saveSendRequest(sendId,receiverId)==1){
                map.put("message","Save Friend Request is successfully!!!");
                map.put("status",true);
                httpStatus=httpStatus.ACCEPTED;
            }else {
                map.put("message","Save Friend Request not successfully!!!");
                map.put("status",false);
                httpStatus=httpStatus.NOT_FOUND;
            }

        }
        catch (Exception e){
            map.put("message","something when wrong");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        }
        return new ResponseEntity<Map<String, Object>>(map,httpStatus);
    }

    //Friend Confirm
    @PutMapping("/confirm/{sendId}/{receiverId}")
    public ResponseEntity<Map<String,Object>> confirmRequest(@PathVariable("sendId") int sendId,@PathVariable("receiverId") int receiverId){

        map=new HashMap<>();
        try {
            if (friendRequestService.confirmRequest(sendId,receiverId)==1){
                map.put("message","Confirm Friend is successfully!!!");
                map.put("status",true);
                httpStatus=httpStatus.ACCEPTED;
            }else {
                map.put("message","Confirm Friend not successfully!!!");
                map.put("status",false);
                httpStatus=httpStatus.NOT_FOUND;
            }

        }
        catch (Exception e){
            map.put("message","something when wrong");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        }
        return new ResponseEntity<Map<String, Object>>(map,httpStatus);
    }

    /// Delete Friend Request
    @DeleteMapping("/delete/{sendId}/{receiverId}")
    public ResponseEntity<Map<String,Object>> deleteRequest(@PathVariable("sendId") int sendId,@PathVariable("receiverId") int receiverId){

        map=new HashMap<>();
        try {
            if (friendRequestService.deleteRequest(sendId,receiverId)==1){
                map.put("message","Delete Friend is successfully!!!");
                map.put("status",true);
                httpStatus=httpStatus.ACCEPTED;
            }else {
                map.put("message","Delete Friend not successfully!!!");
                map.put("status",false);
                httpStatus=httpStatus.NOT_FOUND;
            }

        }
        catch (Exception e){
            map.put("message","something when wrong");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        }
        return new ResponseEntity<Map<String, Object>>(map,httpStatus);
    }

    //Delete Friend from mn mate_list or Unfriend
    @DeleteMapping("/friend/remove/{userId}/{mateId}")
    public ResponseEntity<Map<String,Object>> removeFriend(@PathVariable("userId") int userId,@PathVariable("mateId") int mateId){

        map=new HashMap<>();
        try {
            if (friendRequestService.removeFriend(userId,mateId)==1){
                map.put("message","Unfriend is successfully!!!");
                map.put("status",true);
                httpStatus=httpStatus.ACCEPTED;
            }
            else {
                map.put("message","Unfriend is not successfully!!!");
                map.put("status",false);
                httpStatus=httpStatus.NOT_FOUND;
            }

        }
        catch (Exception e){
            map.put("message","something when wrong");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
            e.printStackTrace();

        }

        return new ResponseEntity<Map<String, Object>>(map,httpStatus);
    }

    @GetMapping(value = "/checkFriend/{userId}/{mateId}")
    public Map<String,Object> checkStatusFriend(@PathVariable("userId") int userId, @PathVariable("mateId") int mateId)
    {
        map=new HashMap<>();
        try {


            if (friendRequestService.checkFriend(userId,mateId)==true){
                map.put("message","All both user are friend!!!!");
                map.put("status",true);
                httpStatus=httpStatus.ACCEPTED;
            }
            else {
                map.put("message","All both user are not friend!!!!");
                map.put("status",false);
                httpStatus=httpStatus.NOT_FOUND;
            }
        }
        catch (Exception e){
            map.put("message","something when wrong");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        }
        return map;
    }


    @GetMapping(value = "/getAllFriendRequest/{reciverId}",produces = {"application/json"})
    public Map<String,Object> getAllFriendRequest(@PathVariable("reciverId") int reciverId){
        map=new HashMap<>();
        List<FriendRequest> getAllFriendRequest=friendRequestService.getAllFriendRequest(reciverId,false);

        if(!getAllFriendRequest.isEmpty()){
            map.put("message","getAllFriendRequest  is successfully!!!");
            map.put("status",true);
            map.put("data",getAllFriendRequest);
        }
        else {
            map.put("message","getAllFriendRequest is not successfully!!!");
            map.put("status",false);

        }

        return map;
    }

    @GetMapping(value = "/getLastFriendRequest/{reciverId}",produces = {"application/json"})
    public Map<String,Object> getLastFriendRequest(@PathVariable("reciverId") int reciverId){
        map=new HashMap<>();
        List<FriendRequest> getLastFriendRequest=friendRequestService.getLastRequest(reciverId,false);

        if(!getLastFriendRequest.isEmpty()){
            map.put("message","getLastFriendRequest  is successfully!!!");
            map.put("status",true);
            map.put("data",getLastFriendRequest);
        }
        else {
            map.put("message","getLastFriendRequest is not successfully!!!");
            map.put("status",false);

        }

        return map;
    }

    @GetMapping("/count/{currentId}")
    public Map<String,Object> countFriendRequest(@PathVariable("currentId") int currentId){
        map=new HashMap<>();
        Integer countFriendRequest=notificationService.countNotificationFriendRequest(currentId);

        if (countFriendRequest>=0){
            map.put("message","countFriendRequest is successfully!!!");
            map.put("status",true);
            map.put("TotalCount",countFriendRequest);
        }

        return map;
    }
}
