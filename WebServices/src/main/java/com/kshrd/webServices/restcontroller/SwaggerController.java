package com.kshrd.webServices.restcontroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/api/swagger/ui")
public class SwaggerController {
    @GetMapping
    public String swagger(){
        return "swagger/index-v2";
    }


}
