package com.kshrd.webServices.restcontroller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Leang on 7/31/2017.
 */
@RestController
@RequestMapping("/api/v1/upload")
public class UploadFileRestController {

    private final Path rootLocation = Paths.get("C:\\Users\\BTB_016\\Desktop\\Prototype");
    private String randomFileName = null;

    @PostMapping
    public ResponseEntity<Map<String, Object>> uploadFile(@RequestParam("file") MultipartFile uploadFile) {
        Map<String, Object> map = new HashMap<>();
        File file = new File(rootLocation.toString());
        if (!file.exists()) {
            if (file.mkdirs()) {
//                create new directory
//                System.out.println("Multiple directories are created!");
            } else {
//                no create directory
//                System.out.println("Failed to create multiple directories!");
            }
        }

        try {
            String uploadedFolder = rootLocation.toString();
            String filePath = saveUploadedFiles(Arrays.asList(uploadFile), uploadedFolder);
            map.put("data", uploadedFolder);
            map.put("path", filePath);
            map.put("filename", randomFileName);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    private String saveUploadedFiles(List<MultipartFile> files, String uploadedFolder) throws IOException {

        for (MultipartFile file : files) {
            if (file.isEmpty()) {
                continue;
            }
            randomFileName = UUID.randomUUID().toString() + ".jpg";
            System.out.println(file.getSize());
            System.out.println(file.getOriginalFilename());
            System.out.println(rootLocation + "/" + randomFileName);

            byte[] bytes = file.getBytes();
            // TODO: write file to uploadedFolder
            Path path = Paths.get(uploadedFolder,
                    randomFileName /* file.getOriginalFilename() */);
            Files.write(path, bytes);
        }
        return rootLocation + "/" + randomFileName;
    }

}
