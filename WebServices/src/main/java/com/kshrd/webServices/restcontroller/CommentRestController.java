package com.kshrd.webServices.restcontroller;

import com.kshrd.models.Comment;
import com.kshrd.models.Pagination;
import com.kshrd.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/comment")
public class CommentRestController {
    private CommentService commentService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * find one comment by id
     * @param id
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("/post")
    public ResponseEntity<Map<String, Object>> findCommentByPostId(
            @RequestParam(defaultValue = "1", value = "id", required = false) Integer id,
            @RequestParam(defaultValue = "0", value = "page", required = false) Integer page,
            @RequestParam(defaultValue = "12", value = "limit", required = false) Integer limit
    ) {
        Pageable pageable = new PageRequest(page, limit, Sort.Direction.ASC, "id");
        Page<Comment> allComments = commentService.findCommentByPostId(id,true,pageable);
        Pagination pagination = new Pagination(allComments.getPageable().getPageNumber(), limit, allComments.getTotalElements(), allComments.getTotalPages());
        Map<String, Object> res = new HashMap<>();
        if (allComments.getContent().isEmpty()) {
            res.put("status", false);
            res.put("message", "no data");
            return new ResponseEntity(res, HttpStatus.NOT_FOUND);
        }
        res.put("status", true);
        res.put("message", "Get Data Successfully");
        res.put("data", allComments.getContent());
        res.put("pagination", pagination);
        return new ResponseEntity(res, HttpStatus.OK);
    }


    /**
     * find one comment by id
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<Map<String, Object>> findCommentById(
            @PathVariable("id") Integer id
    ) {

        Map<String, Object> res = new HashMap<>();
        Comment comment=commentService.findCommentById(id,true);
        if (comment==null) {
            res.put("status", false);
            res.put("message", "no data");
            return new ResponseEntity(res, HttpStatus.NOT_FOUND);
        }
        res.put("status", true);
        res.put("message", "Get Data Successfully");
        res.put("data", comment);
        return new ResponseEntity(res, HttpStatus.OK);
    }

    /**
     * countCommentByPostId
     * @param id
     * @return
     */

    @GetMapping("/count/{id}")
    public ResponseEntity<Map<String, Object>> countCommentByPostId(@PathVariable("id")Integer id){
        Map<String, Object> res = new HashMap<>();
        Integer total=commentService.countCommentByPostId(id,true);
        if(total<=0){
            res.put("status",false);
            res.put("message","no result to count");
            return new ResponseEntity(res, HttpStatus.NOT_FOUND);
        }
        res.put("status",true);
        res.put("message","completed");
        res.put("data",total);
        return new ResponseEntity(res, HttpStatus.OK);

    }


    @GetMapping("/user")
    public ResponseEntity<Map<String, Object>> findCommentByUserId(
            @RequestParam(defaultValue = "1", value = "id", required = false) Integer id,
            @RequestParam(defaultValue = "0", value = "page", required = false) Integer page,
            @RequestParam(defaultValue = "12", value = "limit", required = false) Integer limit
    ) {
        Pageable pageable = new PageRequest(page, limit, Sort.Direction.ASC, "id");
        Page<Comment> allComments = commentService.findCommentByUserId(id,true,pageable);
        Pagination pagination = new Pagination(allComments.getPageable().getPageNumber(), limit, allComments.getTotalElements(), allComments.getTotalPages());
        Map<String, Object> res = new HashMap<>();
        if (allComments.getContent().isEmpty()) {
            res.put("status", false);
            res.put("message", "no data");
            return new ResponseEntity(res, HttpStatus.NOT_FOUND);
        }
        res.put("status", true);
        res.put("message", "Get Data Successfully");
        res.put("data", allComments.getContent());
        res.put("pagination", pagination);
        return new ResponseEntity(res, HttpStatus.OK);
    }


    /**
     * countCommentByUserId
     * @param id
     * @return
     */
    @GetMapping("/count/user/{id}")
    public ResponseEntity<Map<String, Object>> countCommentByUserId(@PathVariable("id")Integer id){
        Map<String, Object> res = new HashMap<>();
        Integer total=commentService.countCommentByUserId(id,true);
        if(total<=0){
            res.put("status",false);
            res.put("message","no result to count");
            return new ResponseEntity(res, HttpStatus.NOT_FOUND);
        }
        res.put("status",true);
        res.put("message","completed");
        res.put("data",total);
        return new ResponseEntity(res, HttpStatus.OK);
    }

    //save Comments
//    {
//        "commentText":"I love you so much",
//            "postUrl":"this sweet word",
//            "status":"true",
//            "uid":"1",
//            "pId":"1"
//
//    }
    @PostMapping("/save")
    public ResponseEntity<Map<String, Object>> saveComment(@RequestBody Comment comment) {
        Map<String, Object> response = new HashMap<>();
        Integer i = commentService.saveComment(comment);
        if (i <= 0) {
            response.put("status", false);
            response.put("message", "no operation");
            response.put("execute", i);
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", true);
        response.put("message", "success");
        response.put("execute", i);
        return new ResponseEntity(response, HttpStatus.OK);
    }


//    request type
//    {
//        "id":"2",
//            "commentText":"hello test here",
//            "postUrl":"hello url"
//    }
    @PutMapping("/")
    public ResponseEntity<Map<String, Object>> updateCommentById(@RequestBody Comment comment) {
        System.out.println("Comment: "+comment.getCommentText()+", "+comment.getPostUrl()+", "+comment.getId());
        Map<String, Object> response = new HashMap<>();
        Integer code = commentService.updateCommentById(comment);
        if (code <= 0) {
            response.put("status", false);
            response.put("message", "no operation");
            response.put("execute", code);
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", true);
        response.put("message", "success");
        response.put("execute", code);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Object>> updateCommentById(@PathVariable("id") Integer id) {
        Map<String, Object> response = new HashMap<>();
        Integer code = commentService.deleteCommentById(id);
        if (code > 0) {
            response.put("status", true);
            response.put("message", "success");
            response.put("execute", code);
            return new ResponseEntity(response, HttpStatus.OK);
        }
        response.put("status", false);
        response.put("message", "no operation");
        response.put("execute", code);
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);

    }
}
