package com.kshrd.webServices.restcontroller;

import com.kshrd.models.FriendRequest;
import com.kshrd.models.Group;
import com.kshrd.models.Notification;
import com.kshrd.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/notification")
public class NotificationController {

    private HttpStatus httpStatus ;
    private Map<String,Object> map;

    @Autowired
    public NotificationService notificationService;


    @PostMapping("/saveNotificationComment/{userId}/{targetUserId}/{postId}/{type}")
    public Map<String,Object> saveNotificationCommentPost(@PathVariable("userId") int userId, @PathVariable("targetUserId") int targetUserId, @PathVariable("postId") int postId, @PathVariable("type") String type){
        map=new HashMap<>();


        if (notificationService.saveNotificationCommentPost(userId,targetUserId,postId,type)==1){
            map.put("message","notification comment save is successfully!!!");
            map.put("status",true);
        }

        return map;
    }

    @PostMapping("/saveNotificationOnUserLike/{userId}/{postId}/{type}")
    public Map<String,Object> saveNotificationOnUserLike( @PathVariable("userId") int userId, @PathVariable("postId") int postId, @PathVariable("type") String type){
        map=new HashMap<>();


        if (notificationService.saveNotificationOnUserLike(userId,postId,type)==1){
            map.put("message","notification like save is successfully!!!");
            map.put("status",true);
        }

        return map;
    }

    @PostMapping("/saveNotificationOnConfirmFriend/{senderId}/{receiverId}/{type}")
    public Map<String,Object> saveNotificationOnConfirmFriend(@PathVariable("senderId") int senderId, @PathVariable("receiverId") int receiverId, @PathVariable("type") String type){
        map=new HashMap<>();


        if (notificationService.saveNotificationOnConfirmFriend(senderId,receiverId,type)==1){
            map.put("message","notification confirm save is successfully!!!");
            map.put("status",true);
        }

        return map;
    }


    @PostMapping("/saveNotificationOnSendFriendRequest/{senderId}/{receiverId}/{type}")
    public Map<String,Object> saveNotificationOnSendFriendRequest(@PathVariable("senderId") int senderId, @PathVariable("receiverId") int receiverId, @PathVariable("type") String type){
        map=new HashMap<>();


        if (notificationService.saveNotificationOnSendFriendRequest(senderId,receiverId,type)==1){
            map.put("message","notification SendFriendRequest save is successfully!!!");
            map.put("status",true);
        }

        return map;
    }


    @PutMapping("/updateConfirm/accept/{senderId}")
    public Map<String,Object> updateTypeAcceptNotificaton(@PathVariable("senderId") int senderId){
        map=new HashMap<>();

        if (notificationService.updateTypeAcceptNotificaton(senderId)==1){
            map.put("message","updateConfirm is_read=true is successfully!!!");
            map.put("status",true);
        }
        else {
            map.put("message","updateConfirm is_read=true not successfully!!!");
            map.put("status",false);
        }
        return map;
    }

    @PutMapping("/updateConfirmSend/{senderId}")
    public Map<String,Object> updateTypeSendNotification(@PathVariable("senderId") int senderId){
        map=new HashMap<>();

        if (notificationService.updateTypeSendNotification(senderId)==1){
            map.put("message","updateConfirmSend is_read=true is successfully!!!");
            map.put("status",true);
        }
        else {
            map.put("message","updateConfirmSend is_read=true not successfully!!!");
            map.put("status",false);
        }
        return map;
    }

    @GetMapping("/getFriendRequestNotificationNumber/{currentUserId}")
    public Map<String,Object> getFriendRequestNotificationNumber(@PathVariable("currentUserId") int currentUserId){
        map=new HashMap<>();
       Integer countFriendRequestNotificationNumber=notificationService.getFriendRequestNotificationNumber(currentUserId);
       try {
           if (countFriendRequestNotificationNumber>=0){
               map.put("message","countFriendRequestNotificationNumber is successfully!!!");
               map.put("status",true);
               map.put("totalCount",countFriendRequestNotificationNumber);
           }
       }

       catch (Exception e)
       {
            map.put("message","something when wrong");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        }
        return map;
    }

    @GetMapping(value = "/getLastConfirmNotification/{targetUser}/{acceptUser}",produces = {"application/json"})
    public Map<String,Object> getLastConfirmNotification(@PathVariable("targetUser") int targetUser,@PathVariable("acceptUser") int acceptUser){
        map=new HashMap<>();
        List<Notification> getLastConfirmNotification=notificationService.getLastConfirmNotification(targetUser,acceptUser);

        if(!getLastConfirmNotification.isEmpty()){
            map.put("message","getLastConfirmNotification  is successfully!!!");
            map.put("status",true);
            map.put("data",getLastConfirmNotification);
        }
        else {
            map.put("message","getLastConfirmNotification is not successfully!!!");
            map.put("status",false);

        }

        return map;
    }

    //create by thon
    @GetMapping("/save/group/{userId}/{postId}/{gId}/{type}")
    public Map<String, Object> saveNotificationPost(@PathVariable("userId") Integer userId, @PathVariable("postId") Integer postId, @PathVariable("type") String type, @PathVariable("gId") Integer gId) {
        // find group name by id
        Group group = new Group();
        group.setId(gId);
        group.setName("Final Group");
        map = new HashMap<>();
        Integer save = notificationService.saveNotificationForGroupPost(userId, group, postId, type);
        if (save > 0) {
            map.put("status", true);
            map.put("message", "success");
            map.put("execute", save);
            return map;
        }
        map.put("status", false);
        map.put("message", "success");
        map.put("execute", save);
        return map;
    }


}
