package com.kshrd.repositories;

import com.kshrd.models.FriendRequest;
import com.kshrd.models.Group;
import com.kshrd.models.Notification;
import com.kshrd.models.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface NotificationRepository extends CrudRepository<Notification,Integer> {

    //save notification when friend friendRequest
    @Modifying
    @Query(value = "insert into mn_notification(user_id,target_user_id,is_read,modified_date,status,type) values (:senderId,:receiverId,false,now(),true,:type)",nativeQuery = true)
    public int saveNotificationOnSendFriendRequest(@Param("senderId") int senderId, @Param("receiverId") int receiverId, @Param("type") String type);

    //save notification when ConfirmFriend
    @Modifying
    @Query(value = "insert into mn_notification(user_id,target_user_id,is_read,modified_date,status,type) values (:senderId,:receiverId,false,now(),true,:type)",nativeQuery = true)
    public int saveNotificationOnConfirmFriend(@Param("senderId") int senderId, @Param("receiverId") int receiverId, @Param("type") String type);

    //save notification when friend friendRequest
    @Modifying
    @Query(value = "insert into mn_notification(user_id,post_id,is_read,modified_date,status,type) values (:userId,:postId,false,now(),true,:type)",nativeQuery = true)
    public int saveNotificationOnUserLike(@Param("userId") int userId, @Param("postId") int postId, @Param("type") String type);


    //Insert comment notification
    @Modifying
    @Query(value = "insert into mn_notification (user_id,target_user_id,post_id,type) values (:userId,:targetUserId,:postId,:type)",nativeQuery = true)
    public int saveNotificationCommentPost(@Param("userId") int userId, @Param("targetUserId") int targetUserId, @Param("postId") int postId, @Param("type") String type);


    // Delete Like notification
    @Modifying
    @Query(value = "delete from mn_notification where user_id=:userId and post_id=:postId and type=:type",nativeQuery = true)
    public int deleteLikeNotification(@Param("userId") int userId, @Param("postId") int postId, @Param("type") String type);

    //select number of adder to show notification number
    @Query(value = "SELECT COUNT  (*) FROM mn_notification WHERE target_user_id =:currentUserId " +
            " AND type ='S' AND is_read='f'",nativeQuery = true)
    public int countNotificationFriendRequest(@Param("currentUserId") int currentUserId);

    @Modifying
    @Query(value = "UPDATE mn_notification SET is_read='t' WHERE type='S' AND target_user_id=:currentUserId ",nativeQuery = true)
    public int updateTypeSendNotification(@Param("currentUserId") int currentUserId);

    @Modifying
    @Query(value = "UPDATE mn_notification SET is_read='t' WHERE type='A' AND target_user_id=:currentUserId",nativeQuery = true)
    public int updateTypeAcceptNotificaton(@Param("currentUserId") int currentUserId);

    @Modifying
    @Query(value = "insert into mn_notification(user_id,target_group_id,target_group_name,type) values (:userId,:#{#group.id},:#{#group.name},:type)",nativeQuery = true)
    int saveNotification(@Param("userId") int userId, @Param("group") Group group, @Param("type") String type);


    @Query(value = "select * from mn_notification f where f.target_user_id=:target and f.user_id=:userAccept and is_read=false and type ='A' ORDER BY id DESC LIMIT 1",nativeQuery = true)
    public List<Notification> getLastConfirmNotification(@Param("target") int target, @Param("userAccept") int userAccept);

    //create by thon
    @Modifying
    @Query(value = "INSERT INTO mn_notification\n" +
            "(\n" +
            "user_id,\n" +
            "target_group_id,\n" +
            "target_group_name,\n" +
            "type,log_date,\n" +
            "modified_date, \n" +
            "is_read,\n" +
            "status,\n" +
            "post_id\n" +
            ")\n" +
            "VALUES\n" +
            "(\n" +
            ":userId,\n" +
            ":#{#group.id},\n" +
            ":#{#group.name},\n" +
            ":type,now(),\n" +
            "now(), \n" +
            "false,\n" +
            "true,\n" +
            ":postId\n" +
            ");",nativeQuery = true)
    public Integer saveNotificationForGroupPost(@Param("userId") int userId, @Param("group") Group group, @Param("postId") int postId, @Param("type") String type);


    @Modifying
    @Query(value = "INSERT INTO mn_notification\n" +
            "(\n" +
            "user_id,\n" +
            "target_user_id,\n" +
            "type,log_date,\n" +
            "modified_date, \n" +
            "is_read,\n" +
            "status,\n" +
            "post_id\n" +
            ")\n" +
            "VALUES\n" +
            "(\n" +
            ":userId,\n" +
            ":mateId,\n" +
            ":type,now(),\n" +
            "now(), \n" +
            "false,\n" +
            "true,\n" +
            ":postId\n" +
            ");",nativeQuery = true)
    public Integer saveNotificationForMatePost(@Param("userId") int userId, @Param("mateId")Integer mateId, @Param("postId") int postId, @Param("type") String type);



}
