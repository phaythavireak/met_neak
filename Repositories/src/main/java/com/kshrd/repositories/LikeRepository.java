package com.kshrd.repositories;

import com.kshrd.models.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository("Like")
@Transactional
public interface LikeRepository extends JpaRepository<Like, Integer> {

    //Find userId in mn_like by post_id

    @Query(value = "select user_id from mn_like where post_id=:postId", nativeQuery = true)
    public List<Integer> getUserIdBypostId(@Param("postId") int postId);


    //Find like

    @Query(value = "select count (*) from mn_like where post_id=:postId and user_id=:userId and status=true  ", nativeQuery = true)
    public int findLike(@Param("userId") int userId, @Param("postId") int postId);

    //Find like no status
    @Query(value = "select count (*) from mn_like where post_id=:postId and user_id=:userId ", nativeQuery = true)
    public int findLikeNostatus(@Param("userId") int userId, @Param("postId") int postId);

    //Save like
    @Modifying
    @Query(value = "insert into mn_like (user_id,post_id,like_date,modified_date) values (:userId,:postId,now(),now())", nativeQuery = true)
    public int saveLike(@Param("userId") int userId, @Param("postId") int postId);

    //Modify like
    @Modifying
    @Query(value = "update mn_like set status=false where user_id=:userId and post_id=:postId", nativeQuery = true)
    public int deleteLike(@Param("userId") int userId, @Param("postId") int postId);


    //Countlikes from post by postId
    @Query(value = "select count (*)  from mn_like where post_id=:postId and status=true ", nativeQuery = true)
    public int countLike(@Param("postId") int postId);

    //Modify like
    @Modifying
    @Query(value = "update mn_like set status=true where user_id=:userId and post_id=:postId", nativeQuery = true)
    public int updateLike(@Param("userId") int userId, @Param("postId") int postId);


    //Countlikes from post by postId
    @Query(value = "select count (*)  from mn_like where post_id=:postId  ", nativeQuery = true)
    public int countNostatus(@Param("postId") int postId);

    //Countlikes By userId

    @Query(value = "select count (*) from mn_like where user_id=:userId and status=true ", nativeQuery = true)
    public int countLikesByUserId(@Param("userId") int userId);


    //Get All like
  /*   @Query(value = "select new Like (l.id,l.post.id,l.likeDate,l.user.id,u.id,u.firstName,u.lastName,u.username,u.role,u.profileUrl) from " +
             "Like l inner join User u on u.id=l.user.id where l.post.id=:postId and l.status=:status" )
     List<Like> findLikeByPostIdAndStatus(@Param("postId") int postId,@Param("status") Boolean status);
*/

    /*@Query(value = "select l from Like l where  status=true")
    public List<Like> getAllLikes(@Param("postId") int postId);*/

     public List<Like> findLikeByPostIdAndStatus(@Param("postId") Integer postId, @Param("status") Boolean status);


};