package com.kshrd.repositories;

import com.kshrd.models.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryRepository {

    List<Category> categories = new ArrayList<>();

    public List<Category> category() {
        categories.add(new Category(1, "AAA"));
        categories.add(new Category(2, "BBB"));
        categories.add(new Category(3, "CCC"));
        categories.add(new Category(4, "DDD"));
        System.out.println("Runed Repository");
        return categories;
    }
}
