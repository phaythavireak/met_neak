package com.kshrd.repositories;


import com.kshrd.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<Location,Integer> {


    @Query(value = "select distinct(l.provinceName) from Location l order by l.provinceName asc")
    List<Location> getallprovice();



    @Query(value = "select l.id,l.districtName from Location l where l.provinceName = :provinceName order by l.districtName asc")
    List<Location> findDistrictNameByProvinceName(@Param("provinceName") String provinceName);










}
