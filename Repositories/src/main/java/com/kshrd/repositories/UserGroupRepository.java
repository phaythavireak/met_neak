package com.kshrd.repositories;


import com.kshrd.models.UserSchool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserGroupRepository extends JpaRepository<UserSchool,Integer> {




    @Modifying
    @Transactional
    @Query(value = "delete from mn_user_group where user_id = :userId",nativeQuery = true)
    int deleteUserGroup(@Param("userId") int userId);


    @Modifying
    @Transactional
    @Query(value = "insert into mn_user_group(user_id,group_id,created_date,modified_date) values(:#{#userId},:#{#groupId},now(),now())",nativeQuery = true)
    int addUserToGroup(@Param("groupId") int groupId, @Param("userId") int userId);

}
