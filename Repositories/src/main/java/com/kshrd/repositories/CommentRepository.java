package com.kshrd.repositories;


import com.kshrd.models.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    /**
     * find comment by post id with pagination
     * @param id
     * @return
     */
    public Page<Comment> findCommentByPostIdAndStatus(@Param("id")Integer id,@Param("status")Boolean status, Pageable pageable);


    /**
     * find one comment by comment id
     * @param id
     * @return
     */
    public Comment findCommentByIdAndStatus(@Param("id")Integer id,@Param("status")Boolean status);


    /**
     * countCommentByPostId
     * @param id
     * @return
     */
    public Integer countCommentByPostIdAndStatus(@Param("id")Integer id,@Param("status")Boolean status);

    /**
     * findCommentByUserId
     * @param id
     * @param pageable
     * @return
     */
    public Page<Comment> findCommentByUserIdAndStatus(@Param("id")Integer id,@Param("status")Boolean status, Pageable pageable);


    /**
     * countCommentByUserId
     * @param id
     * @return
     */
    public Integer countCommentByUserIdAndStatus(@Param("id")Integer id,@Param("status")Boolean status);


    /**
     * save comments
     * @param comment
     * @return
     */
    @Modifying
    @Query(value = "insert into mn_comment(comment_date,comment_text,modified_date,post_url,status,post_id,user_id)" +
            "values(now(),:#{#comment.commentText},now(),:#{#comment.postUrl},:#{#comment.status},:#{#comment.pId},:#{#comment.uid})",nativeQuery = true)
    public Integer saveComment(@Param("comment")Comment comment);


    /**
     * update comment
     * @param comment
     * @return
     */
    @Modifying
    @Query(value = "UPDATE mn_comment SET comment_text=:#{#comment.commentText}, modified_date=now(), post_url=:#{#comment.postUrl} WHERE id=:#{#comment.id}",nativeQuery = true)
    public Integer updateCommentById(@Param("comment")Comment comment);


    /**
     * delete comment by id
     * @param id
     * @return
     */
    public Integer deleteCommentById(@Param("id")Integer id);

    @Query(value = "SELECT DISTINCT c.user_id FROM mn_comment c WHERE c.post_id=:postId AND c.status=true and c.user_id not in (:ownerId)",nativeQuery = true)
    public List<Integer> getUserIdCommentOnePostId(@Param("postId")Integer postId,@Param("ownerId")Integer ownerId);


    @Query(value = "SELECT\n" +
            "c.id\n" +
            "FROM mn_comment c \n" +
            "WHERE c.status=true\n" +
            "ORDER BY c.id DESC\n" +
            "LIMIT 1;",nativeQuery = true)
    public Integer getCurrentComment();


    @Query(value = "select * from mn_comment m order by m.id desc limit 1",nativeQuery = true)
    public Comment getLastComment();
}
