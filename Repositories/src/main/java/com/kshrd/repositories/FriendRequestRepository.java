package com.kshrd.repositories;

import com.kshrd.models.FriendRequest;
import org.hibernate.validator.constraints.EAN;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional

public interface FriendRequestRepository  extends CrudRepository<FriendRequest,Integer> {


    //Save Friend Request to database
    @Modifying
    @Query(value = "insert into mn_friend_request(user_id,receiver_id,received_date,status) values (:senderId,:receiverId,now(),true )",nativeQuery = true)
    public int saveSendRequest(@Param("senderId") int senderId, @Param("receiverId") int receiverId);

    //confirm FriendRequest
    @Modifying
    @Query(value ="update mn_friend_request set confirm =TRUE where user_id=:senderId and receiver_id=:receiverId" ,nativeQuery = true)
    public int confirmRequest(@Param("senderId") int senderId, @Param("receiverId") int receiverId);

    //save mate list
    @Modifying
    @Query(value = "INSERT INTO  mn_mate_list(user_id,mate_id,status,relationship_date) values(:userId,:mateId,TRUE,now())",nativeQuery = true)
    public int saveMate(@Param("userId") int userId, @Param("mateId") int mateId);

    @Modifying
    @Query(value = "DELETE FROM mn_friend_request WHERE user_id =:senderId AND receiver_id =:receiverId ",nativeQuery = true)
    public int deleteFriendRequest(@Param("senderId") int senderId, @Param("receiverId") int receiverId);

    @Modifying
    @Query(value = "delete from mn_mate_list where user_id=:userId and mate_id=:mateId",nativeQuery = true)
    public int removeFriend(@Param("userId") int userId,@Param("mateId") int mateId);

    @Modifying
    @Query(value = "select status from mn_mate_list where user_id=:userId and mate_id=:mateId",nativeQuery = true)
    public Boolean checkFriend (@Param("userId") int userId,@Param("mateId") int mateId);
    @Modifying
    @Query(value = "select * from mn_friend_request f where f.receiver_id=:reciverId and f.confirm=:confirm",nativeQuery = true)
    public List<FriendRequest> getAllRequest(@Param("reciverId") int reciverId,@Param("confirm") Boolean confirm);
    @Modifying
    @Query(value = "select * from mn_friend_request f where f.receiver_id=:reciverId and f.confirm=:confirm ORDER BY id DESC LIMIT 1",nativeQuery = true)
    public  List<FriendRequest> getLastRequest(@Param("reciverId") int reciverId,@Param("confirm") Boolean confirm);

}
