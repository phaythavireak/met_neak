package com.kshrd.repositories;



import com.kshrd.models.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group,Integer>{




    @Query(value = "SELECT g FROM Group g WHERE g.name = :name || ',' || :level || ',' || :year")
    Group findGroupBySchool(@Param("name") String name, @Param("level") String level, @Param("year") int year);


    @Override
    <S extends Group> S save(S s);


    //create by thon
    @Query(value = "select g FROM Group g where g.id=:id")
    public Group findGroupById(@Param("id") Integer id);

}
