package com.kshrd.repositories;

import com.kshrd.models.Post;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface PostRepository extends PagingAndSortingRepository<Post, Integer> {


    @Query(value = "select p.id,u.id,u.firstName,u.lastName,u.role,u.profileUrl,p.content,p.photoUrl,p.createdDate,p.group.id " +
            "from Post p join User u on p.user.id=u.id where p.id=:postId and p.status=true ")
    public Post findPostById(@Param("postId") int postId);


    /**
     * 1. savePostInGroup
     */
    @Modifying
    @Query(value = "insert into mn_post" +
            "(content," +
            "created_date," +
            "modified_date," +
            "photo_url," +
            "status," +
            "group_id," +
            "user_id)" +
            "VALUES" +
            "(:#{#post.content},now(),now()," +
            ":#{#post.photoUrl},:#{#post.status}," +
            ":#{#post.gId},:#{#post.uId})", nativeQuery = true)
    public Integer savePostInGroup(@Param("post") Post post);


    /**
     * 2. findPostsByGroupId
     */
    @Query(value = "SELECT * FROM mn_post p WHERE p.group_id=:groupId AND p.status=TRUE", nativeQuery = true)
    public Page<Post> findPostsByGroupId(@Param("groupId") Integer groupId, Pageable pageable);


    /**
     * 3. findPostsByAllGroupId
     */
    @Query(value = "select * from mn_post p where p.group_id in (:g1,:g2,:g3) AND p.status=TRUE ", nativeQuery = true)
    public Page<Post> findPostsByAllGroupId(@Param("g1") Integer g1, @Param("g2") Integer g2, @Param("g3") Integer g3, Pageable pageable);


    /**
     * 4. countPostByGroupId
     */
    public Integer countPostByGroupIdAndStatus(@Param("g") Integer g, @Param("status") Boolean status);


    /**
     * 5. countPostByAllGroupId
     */
    @Query(value = "select count(*) from mn_post p where p.group_id in (:g1,:g2,:g3) and p.status=true ", nativeQuery = true)
    public Integer countPostByAllGroup(@Param("g1") Integer g1, @Param("g2") Integer g2, @Param("g3") Integer g3);


    /**
     * 6. findAllPosts
     */
//    @Query(value = "SELECT p.content,u.id from post_entity p inner join user_entity u on u.id=p.user.id where p.status=true")
//    public List<Post> findAllPost();
    @Query("SELECT p FROM Post p order by p.id desc")
    public List<Post> findAllPost();

    /**
     * 7. findPostsByUserId
     */
    public Page<Post> findPostByUserId(@Param("uid") Integer uid, Pageable pageable);


    /**
     * 8. countPostsByUserId
     */
    @Query(value = "Select count(*) from mn_post p where p.user_id=:uid and p.status=TRUE", nativeQuery = true)
    public Integer countPostByUserId(@Param("uid") Integer uid);


    /**
     * 9. findPostById
     */
//    @Query(value = "SELECT p FROM Post p WHERE p.id=:id AND p.status=:status")
    public Post findPostByIdAndStatus(@Param("id") Integer id, @Param("status") Boolean status);


    /**
     * 10. filterPostsByGroupId Date and status
     */
    @Query(value = "SELECT * from mn_post p where p.created_date BETWEEN '2015-09-26' AND '2019-02-1' AND p.status=TRUE AND p.group_id=1", nativeQuery = true)
    public Page<Post> filterPostsByGroupId(@Param("gId") Integer gId, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("status") Boolean status, Pageable pageable);


    /**
     * 11. findPostsByMateId
     */
    public Page<Post> findPostByMateIdAndStatus(@Param("mid") Integer mid, @Param("status") Boolean status, Pageable pageable);


    /**
     * 12. savePostFor Mate
     */
    @Modifying
    @Query(value = "insert into mn_post" +
            "(content," +
            "created_date," +
            "modified_date," +
            "photo_url," +
            "status," +
            "mate_id," +
            "user_id)" +
            "VALUES" +
            "(:#{#post.content},now(),now()," +
            ":#{#post.photoUrl},:#{#post.status}," +
            ":#{#post.mateId},:#{#post.uId})", nativeQuery = true)
    public Integer savePostFormMate(@Param("post") Post post);


    /**
     * 13. delete Post
     */
    @Modifying
    @Query(value = "UPDATE mn_post SET status = false, modified_date = now() WHERE id = :id", nativeQuery = true)
    public Integer deletePostById(@Param("id") Integer id);


    /**
     * 14. update Post
     */
    @Modifying
    @Query(value = "UPDATE mn_post set content=:#{#post.content},modified_date=now(),photo_url=:#{#post.photoUrl} where id=:#{#post.id}", nativeQuery = true)
    public Integer updatePostById(@Param("post") Post post);


//    @Query(value = "")
//    public List<Integer> getUserIdByPostId(@Param("postId")Integer postId);\

    @Query(value = "select p.id from mn_post p order by p.id desc limit 1", nativeQuery = true)
    public Integer getCurrentPostId();


    @Query(value = "select * from mn_post m order by m.id desc limit 1",nativeQuery = true)
    public Post getLastPost();


}


