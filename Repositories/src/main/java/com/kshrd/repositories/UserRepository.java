package com.kshrd.repositories;

import com.kshrd.models.School;
import com.kshrd.models.User;
import com.kshrd.models.from.FrmsocailMedia;
import javafx.scene.control.Pagination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {



    @Query(value = "select u.id , u.firstName, u.lastName, u.username, u.gender, u.dob, u.locations, l.provinceName, l.districtName, u.status, \n" +
            "            u.email, u.facebookId, u.createDate ,\n" +
            "            u.modifiedDate , u.description, u.role, u.profileUrl, u.coverUrl, u.status  from \n" +
            "            User u join Location l on u.locations = l.id")
    List<User> getall();


    @Query(value = "select count(u.id) from User u")
    int countAllUser();

    @Modifying
    @Transactional
    @Query(value =
            "insert INTO mn_user (" +
                    " first_name," +
                    " last_name," +
                    " username," +
                    " role," +
                    " gender," +
                    " dob," +
                    " location_id," +
                    " email," +
                    " facebook_id," +
                    " profile_url," +
                    " cover_url" +
                    " ) VALUES " +
                    "(" +
                    " :#{#user.firstName}," +
                    " :#{#user.lastName}," +
                    " :#{#user.username}," +
                    " :#{#user.role}," +
                    " :#{#user.gender}," +
                    " :#{#user.dob}," +
                    " :#{#user.locations.id}," +
                    " :#{#user.email}," +
                    " :#{#user.facebookId}," +
                    " :#{#user.profileUrl}," +
                    " :#{#user.coverUrl}" +
                    " )",nativeQuery = true
    )

    int saveUser(@Param("user") User user);





    @Query(value = "select u.facebookId from User u where u.facebookId = :facebookId")
    List<User> findUserByFacebookId(@Param("facebookId") String facebookId);


    @Query(value = "select u.id , u.firstName, u.lastName, u.username, u.gender, u.dob, u.locations, l.provinceName, l.districtName, u.status, \n" +
            "            u.email, u.facebookId, u.createDate ,\n" +
            "            u.modifiedDate , u.description, u.role, u.profileUrl, u.coverUrl, u.status  from \n" +
            "            User u join Location l on u.locations = l.id where u.id = :userId")
    List<User> findUserById(@Param("userId") int userId);


    @Query(value = "SELECT  u.role,l.provinceName, l.districtName, u.firstName,u.lastName,u.dob,u.gender,u.profileUrl, " +
            "u.status FROM User u join MateList ml on u.id = ml.mate.id\n" +
            "            join Location l on u.locations.id = l.id \n" +
            "            where ml.user.id = :userId")
    List<User> findAllUserFriendByUserId(@Param("userId") int userId);


    @Query(value = "SELECT count(ml.mate.id) FROM User u join MateList ml on u.id = ml.mate.id " +
            "where u.id = :user_id")
    int countAllUserFriend(@Param("user_id") int user_id);

    List<User> findAllByEmail(@Param("email") String email);



    @Modifying
    @Transactional
    @Query(value =
            "INSERT INTO mn_user (" +
                    " first_name," +
                    " last_name," +
                    " gender, " +
                    " username," +
                    " email," +
                    " facebook_id," +
                    " profile_url," +
                    " role" +
                    " )" +
                    " VALUES" +
                    " (" +
                    " :#{#user.firstName}," +
                    " :#{#user.lastName}," +
                    " :#{#user.gender}," +
                    " :#{#user.userName}," +
                    " :#{#user.email}," +
                    " :#{#user.facebookId}," +
                    " :#{#user.profilePhotoUrl}," +
                    " 'U'" +
                    " )",nativeQuery = true
    )

    int saveUserSocailmedia(@Param("user") FrmsocailMedia frmsocailMedia);


    @Modifying
    @Transactional
    @Query(value = "UPDATE mn_user" +
            " SET" +
            " profile_url = :#{#profileUrl}," +
            " cover_url = :#{#coverUrl}" +
            " WHERE" +
            " id = :#{#userId}",nativeQuery = true)
    int updateUserPhoto(@Param("profileUrl") String profileUrl, @Param("coverUrl") String coverUrl, @Param("userId") int userId);





    @Query(value = "SELECT u.id, u.dob, u.role, u.gender, u.firstName, u.lastName, u.profileUrl, l.provinceName, l.districtName FROM User u \n" +
            "                        inner JOIN UserGroup ug ON u.id = ug.user.id inner JOIN Location l ON u.locations.id = l.id  WHERE ug.group.id = :groupId")

    List<User> findAllUserInGroup(@Param("groupId") int groupId);


    @Query(value = "SELECT count(U.id) FROM User U \n" +
            "            JOIN UserGroup UG ON U.id = UG.user.id JOIN Location L ON U.locations.id = L.id  WHERE UG.group.id =:groupId")
    int countAllUserInGroup(@Param("groupId")int groupId);



    @Query(value = "SELECT count(n.user.id) from Notification n where n.user.id = :target_user_id")
    int countNotificationByTargetUserId(@Param("target_user_id") int target_user_id);


    @Modifying
    @Transactional
    @Query(value = "update mn_user set first_name = :#{#user.firstName},last_name=:#{#user.lastName},gender=:#{#user.gender},dob = :#{#user.dob},location_id = :#{#user.locations.id},modified_date = now() where id = :#{#user.id}\n" +
            "",nativeQuery = true)
    int updateUserById(@Param("user") User user);


    @Modifying
    @Transactional
    @Query(value = "update mn_user set first_name = :#{#user.firstName},last_name=:#{#user.lastName},gender=:#{#user.gender},dob = :#{#user.dob},location_id = :#{#user.locations.id},modified_date = now() where facebook_id = :#{#user.facebookId}\n" +
            "",nativeQuery = true)
    int registerUser(@Param("user") User user);



    @Query(value = "SELECT u.id,u.firstName,u.lastName,u.profileUrl FROM User  u JOIN MateList l on u.id = l.mate.id WHERE u.id =:userId")
    List<User> findMatesByUserId(@Param("userId") int userId);


//    @Query(value = "SELECT count(n) from Notification n WHERE n.isRead=false and type <> 'A' and type <> 'S' and n.user.id <> :userId and n.users.id=:userId")
//    int countNotificationByTargetGroupId(@Param("userId") int userId);



    @Query(value = "select count(ml) from MateList ml where ml.user.id = :userId and ml.mate.id = :mateId and status = true")
    int checkUserAndMateById(@Param("userId") int userId, @Param("mateId") int mateId);



    @Query(value = "select COUNT(u.id) from User u join Location l on u.locations.id = l.id join UserSchool us on u.id = us.user.id join School s on s.id = us.school.id where s.name like '%' || :school || '%' and s.level = :level")
    int countAllUserBySchoolAndLevel(@Param("school") String school, @Param("level") String level);


    @Query(value = "select u.id,u.firstName,u.lastName,u.gender,u.dob,u.locations.id, l.provinceName, l.districtName, l.status,u.email, u.facebookId, u.description, u.role,u.profileUrl, u.coverUrl, u.status from User u join Location l on u.locations.id = l.id join UserSchool us on u.id = us.user.id join School s on s.id = us.school.id where s.name like '%' || :school || '%' and s.level = :level")
    List<User> findAllUserBySchoolAndLevel(@Param("school") String school, @Param("level") String level);

    @Modifying
    @Transactional
    @Query(value = "UPDATE mn_notification SET is_read=true WHERE type <> 'A' and type <> 'S' and user_id <> :#{#userId} and target_user_id=:#{#userId}",nativeQuery = true)
    int updateIsReadNotification(@Param("userId") int userId);












}
