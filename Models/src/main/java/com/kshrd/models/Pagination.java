package com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Pagination implements Serializable{

    @JsonProperty("page")
    private Integer page;

    @JsonProperty("limit")
    private Integer limit;

    @JsonProperty("total_element")
    private Long totalElement;

    @JsonProperty("total_page")
    private Integer totalPage;

    public Pagination() {
    }

    public Pagination(Integer page, Integer limit, Long totalElement, Integer totalPage) {
        this.page = page;
        this.limit = limit;
        this.totalElement = totalElement;
        this.totalPage = totalPage;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Long getTotalElement() {
        return totalElement;
    }

    public void setTotalElement(Long totalElement) {
        this.totalElement = totalElement;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
}
