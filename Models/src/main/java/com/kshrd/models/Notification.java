package com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Table;
@Entity
@Table(name = "mn_notification")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String targetGroupName;
    private String type;
    private Timestamp logDate;
    private Timestamp modifiedDate;
    private Boolean isRead;
    private Boolean status;
    private String notificationUrl;

    @ManyToOne
    @JoinColumn(name = "target_user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user_id;


    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "target_group_id")
    private Group group;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @JsonIgnore
    @OneToMany(
            mappedBy = "notification",
            cascade = CascadeType.ALL
    )
    private List<ReadNotification> readNotifications;


    public Notification() {
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", targetGroupName='" + targetGroupName + '\'' +
                ", type='" + type + '\'' +
                ", logDate=" + logDate +
                ", modifiedDate=" + modifiedDate +
                ", isRead=" + isRead +
                ", status=" + status +
                ", notificationUrl='" + notificationUrl + '\'' +
                ", user=" + user +
                ", user_id=" + user_id +
                ", group=" + group +
                ", post=" + post +
                ", readNotifications=" + readNotifications +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTargetGroupName() {
        return targetGroupName;
    }

    public void setTargetGroupName(String targetGroupName) {
        this.targetGroupName = targetGroupName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Timestamp getLogDate() {
        return logDate;
    }

    public void setLogDate(Timestamp logDate) {
        this.logDate = logDate;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getNotificationUrl() {
        return notificationUrl;
    }

    public void setNotificationUrl(String notificationUrl) {
        this.notificationUrl = notificationUrl;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser_id() {
        return user_id;
    }

    public void setUser_id(User user_id) {
        this.user_id = user_id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public List<ReadNotification> getReadNotifications() {
        return readNotifications;
    }

    public void setReadNotifications(List<ReadNotification> readNotifications) {
        this.readNotifications = readNotifications;
    }

    public Notification(String targetGroupName, String type, Timestamp logDate, Timestamp modifiedDate, Boolean isRead, Boolean status, String notificationUrl, User user, User user_id, Group group, Post post, List<ReadNotification> readNotifications) {

        this.targetGroupName = targetGroupName;
        this.type = type;
        this.logDate = logDate;
        this.modifiedDate = modifiedDate;
        this.isRead = isRead;
        this.status = status;
        this.notificationUrl = notificationUrl;
        this.user = user;
        this.user_id = user_id;
        this.group = group;
        this.post = post;
        this.readNotifications = readNotifications;
    }

}
