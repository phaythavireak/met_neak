package com.kshrd.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PrimaryId implements Serializable {

    @Column
    private Integer first;

    @Column
    private Integer second;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrimaryId primaryId = (PrimaryId) o;
        return Objects.equals(first, primaryId.first) &&
                Objects.equals(second, primaryId.second);
    }

    @Override
    public int hashCode() {

        return Objects.hash(first, second);
    }
}
