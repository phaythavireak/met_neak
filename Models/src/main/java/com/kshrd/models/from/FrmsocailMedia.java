package com.kshrd.models.from;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FrmsocailMedia {




    @JsonProperty("first_name")
    private String firstName;



    @JsonProperty("last_name")
    private String lastName;

    private String gender;

    private String email;

    @JsonProperty("profile_photo_url")
    private String profilePhotoUrl;

    private String userName;

    @JsonProperty("facebook_id")
    private String facebookId;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public FrmsocailMedia() {

    }

    public FrmsocailMedia(String firstName, String lastName, String gender, String email, String profilePhotoUrl, String userName, String facebookId) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.profilePhotoUrl = profilePhotoUrl;
        this.userName = userName;
        this.facebookId = facebookId;
    }

}
