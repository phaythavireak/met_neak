package com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;
import javax.persistence.Table;
@Entity
@Table(name = "mn_school")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class School {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String level;
    private Boolean status;

    @OneToMany(
            mappedBy = "school",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<UserSchool> userSchools;

    @ManyToOne(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            optional = false
    )
    @JoinColumn(name = "location_id")
    @JsonIgnore
    private Location locations;


    @Override
    public String toString() {
        return "School{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", level='" + level + '\'' +
                ", status=" + status +
                ", userSchools=" + userSchools +
                ", locations=" + locations +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<UserSchool> getUserSchools() {
        return userSchools;
    }

    public void setUserSchools(List<UserSchool> userSchools) {
        this.userSchools = userSchools;
    }

    public Location getLocations() {
        return locations;
    }

    public void setLocations(Location locations) {
        this.locations = locations;
    }

    public School() {

    }

    public School(String name, String level, Boolean status, List<UserSchool> userSchools, Location locations) {

        this.name = name;
        this.level = level;
        this.status = status;
        this.userSchools = userSchools;
        this.locations = locations;
    }
}
