package com.kshrd.models;

import javax.persistence.*;
import java.sql.Timestamp;
import javax.persistence.Table;
@Entity
@Table(name = "mn_user_group")
public class UserGroup {
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "first",column = @Column(name = "user_id")),
            @AttributeOverride(name = "second",column = @Column(name = "group_id"))
    })
    private PrimaryId primaryId;

    @ManyToOne(
            targetEntity = User.class,
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    @MapsId("first")
    private User user;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @MapsId("second")
    private Group group;

    private Timestamp createdDate;
    private Timestamp modifiedDate;
    private Boolean status;


    @Override
    public String toString() {
        return "UserGroup{" +
                "primaryId=" + primaryId +
                ", user=" + user +
                ", group=" + group +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", status=" + status +
                '}';
    }

    public PrimaryId getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(PrimaryId primaryId) {
        this.primaryId = primaryId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public UserGroup() {

    }

    public UserGroup(PrimaryId primaryId, User user, Group group, Timestamp createdDate, Timestamp modifiedDate, Boolean status) {

        this.primaryId = primaryId;
        this.user = user;
        this.group = group;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.status = status;
    }
}
