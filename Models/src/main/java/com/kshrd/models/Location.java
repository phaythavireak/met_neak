package com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Table;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "mn_location")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String provinceName;
    private String districtName;
    private Boolean status;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "locations",
            cascade = CascadeType.ALL
    )
    @JsonIgnore
    private List<User> users;

    @OneToMany(
            mappedBy = "locations",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @JsonIgnore
    private List<School> schools;


    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", provinceName='" + provinceName + '\'' +
                ", districtName='" + districtName + '\'' +
                ", status=" + status +
                ", users=" + users +
                ", schools=" + schools +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<School> getSchools() {
        return schools;
    }

    public void setSchools(List<School> schools) {
        this.schools = schools;
    }

    public Location() {

    }

    public Location(String provinceName, String districtName, Boolean status, List<User> users, List<School> schools) {

        this.provinceName = provinceName;
        this.districtName = districtName;
        this.status = status;
        this.users = users;
        this.schools = schools;
    }
}
