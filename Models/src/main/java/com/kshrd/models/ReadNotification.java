package com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.sql.Timestamp;
import javax.persistence.Table;
@Entity
@Table(name = "mn_read_notification")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class ReadNotification {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "first",column = @Column(name = "user_id")),
            @AttributeOverride(name = "second",column = @Column(name = "notification_id"))
    })
    private PrimaryId primaryId;

    private Timestamp readDate;

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @MapsId("first")
    private User user;


    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @MapsId("first")
    private Notification notification;

}
