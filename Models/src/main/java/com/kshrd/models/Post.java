package com.kshrd.models;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Table;

@Entity(name = "Post")
@Table(name = "mn_post")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Post implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String content;
    private String photoUrl;
    private Timestamp createdDate;
    private Timestamp modifiedDate;
    private Boolean status;
    private Integer mateId;

    @JsonIgnore
    @Transient
    private Integer uId;
    @JsonIgnore
    @Transient
    private Integer gId;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "group_id",nullable = false
    )
    private Group group;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @JsonIgnore
    @OneToMany(
            mappedBy = "post",
            fetch = FetchType.LAZY
    )
    private List<Like> likes=new ArrayList<>();



//    @JsonBackReference(value = "comments")
    @JsonIgnore
    @OneToMany(
            mappedBy = "post",
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    private List<Comment> comments = new ArrayList<>();

    @JsonIgnore
    @OneToMany(
            mappedBy = "post",
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Notification> notifications=new ArrayList<>();

    public Post() {
    }

    public Post(String content, String photoUrl, Timestamp createdDate, Timestamp modifiedDate, Boolean status) {
        this.content = content;
        this.photoUrl = photoUrl;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getMateId() {
        return mateId;
    }

    public void setMateId(Integer mateId) {
        this.mateId = mateId;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Integer getgId() {
        return gId;
    }

    public void setgId(Integer gId) {
        this.gId = gId;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Like> getLikes() {
        return likes;
    }

    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }
}
