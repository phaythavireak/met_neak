package com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "mn_mate_list")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class MateList {
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "first",column = @Column(name = "user_id")),
            @AttributeOverride(name = "second",column = @Column(name = "mate_id"))
    })
    private PrimaryId primaryId;

    private Timestamp relationshipDate;
    private Boolean status;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            optional = false
    )
    @MapsId("first")
    private User user;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            optional = true
    )
    @MapsId("second")
    private User mate;



    public MateList(PrimaryId primaryId, Timestamp relationshipDate, Boolean status, User user) {
        this.primaryId = primaryId;
        this.relationshipDate = relationshipDate;
        this.status = status;
        this.user = user;
    }

    @Override
    public String toString() {
        return "MateList{" +
                "primaryId=" + primaryId +
                ", relationshipDate=" + relationshipDate +
                ", status=" + status +
                ", user=" + user +
                '}';
    }

    public MateList() {
    }

    public PrimaryId getPrimaryId() {

        return primaryId;
    }

    public void setPrimaryId(PrimaryId primaryId) {
        this.primaryId = primaryId;
    }

    public Timestamp getRelationshipDate() {
        return relationshipDate;
    }

    public void setRelationshipDate(Timestamp relationshipDate) {
        this.relationshipDate = relationshipDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
