package com.kshrd.services;

import com.kshrd.models.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CommentService {

    /**
     * find comment by post id with pagination
     * @param id
     * @return
     */
    public Page<Comment> findCommentByPostId(Integer id,Boolean status, Pageable pageable);

    /**
     * find one comment by id
     * @param id
     * @return
     */
    public Comment findCommentById(Integer id,Boolean status);


    /**
     * countCommentByPostId
     * @param id
     * @return
     */
    public Integer countCommentByPostId(Integer id,Boolean status);


    /**
     * findCommentByUserId
     * @param id
     * @param pageable
     * @return
     */
    public Page<Comment> findCommentByUserId(Integer id,Boolean status,Pageable pageable);

//    countCommentByUserId
    public Integer countCommentByUserId(Integer id,Boolean status);


    public Integer saveComment(Comment comment);

    /**
     * update Post id
     */
    public Integer updateCommentById(Comment comment);

    /**
     * update Post id
     */
    public Integer deleteCommentById(Integer id);
}
