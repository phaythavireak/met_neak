package com.kshrd.services;

import com.kshrd.models.OneSignalPushNotifcation;

import java.util.List;

public interface OneSignalPushNotificationService {

    /**
     * saveOneDevice
     */
    public Integer saveOneDevice(Integer userId, String playerId);

    // check existing device
    public OneSignalPushNotifcation findOneDevice(String playerId);

    public List<String> findDeviceByUserId(Integer userId);

    public List<String> findOneSignalPushNotifcationByGroupId(Integer gId);

    public List<String> findOneSignalPushNotifcationForComment(Integer postId, Integer userId);


}
