package com.kshrd.services.serviceImpl;
import com.kshrd.models.Group;
import com.kshrd.models.Notification;
import com.kshrd.models.User;
import com.kshrd.repositories.NotificationRepository;
import com.kshrd.repositories.UserRepository;
import com.kshrd.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class NotificationServiceImpl implements NotificationService {


    @Autowired
    public NotificationServiceImpl(NotificationRepository notificationRepository, UserRepository userRepository) {
        this.notificationRepository = notificationRepository;
        this.userRepository = userRepository;
    }

    private NotificationRepository notificationRepository;
    private UserRepository userRepository;

    @Override
    public int deleteLikeNotification(int userId, int postId, String type) {
        return notificationRepository.deleteLikeNotification(userId,postId,type);
    }

    @Override
    public int saveNotificationCommentPost(int userId, int targetUserId, int postId, String type) {
        return notificationRepository.saveNotificationCommentPost(userId,targetUserId,postId,type);
    }

    @Override
    public int saveNotificationOnUserLike(int userId, int postId, String type) {
        return notificationRepository.saveNotificationOnUserLike(userId,postId,type);
    }

    @Override
    public int saveNotificationOnConfirmFriend(int senderId, int receiverId, String type) {
        return notificationRepository.saveNotificationOnConfirmFriend(senderId,receiverId,type);
    }

    @Override
    public int saveNotificationOnSendFriendRequest(int senderId, int receiverId, String type) {
        return notificationRepository.saveNotificationOnSendFriendRequest(senderId,receiverId,type);
    }



    @Override
    public Integer countNotificationFriendRequest(int currentUserId) {
        return notificationRepository.countNotificationFriendRequest(currentUserId);
    }

    @Override
    public List<User> getAcceptedRequest(int currentUserId) {
        return null;
    }

    @Override
    public List<User> getAllAddFriendRequestNotification(int selfId) {
        return null;
    }


    @Override
    public Integer getFriendRequestNotificationNumber(int currentUserId) {
        return  notificationRepository.countNotificationFriendRequest(currentUserId);
    }

    @Override
    public int updateTypeSendNotification(int currentUserId) {
        return notificationRepository.updateTypeSendNotification(currentUserId);
    }

    @Override
    public int updateTypeAcceptNotificaton(int currentUserId) {
        return notificationRepository.updateTypeAcceptNotificaton(currentUserId);
    }


    public NotificationServiceImpl(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @Override
    public boolean saveNotificationWhenUserJoinGroup(int userId, Group group) {
        if (notificationRepository.saveNotification(userId,group,"J")==1)
        {
            return true;
        }
        return false;
    }

    @Override
    public List<Notification> getLastConfirmNotification(int target, int userAccept) {
        return notificationRepository.getLastConfirmNotification(target,userAccept);
    }

    //create by thon
    @Override
    public Integer saveNotificationForGroupPost(int userId, Group group, int postId, String type) {
        return notificationRepository.saveNotificationForGroupPost(userId,group,postId,type);
    }

    @Override
    public Integer saveNotificationForMatePost(int userId, Integer mateId, int postId, String type) {
        return notificationRepository.saveNotificationForMatePost(userId,mateId,postId,type);
    }
}
