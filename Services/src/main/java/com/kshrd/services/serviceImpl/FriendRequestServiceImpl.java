package com.kshrd.services.serviceImpl;
import com.kshrd.controller.SocketIoController;
import com.kshrd.models.FriendRequest;
import com.kshrd.repositories.FriendRequestRepository;
import com.kshrd.repositories.NotificationRepository;
import com.kshrd.services.FriendRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FriendRequestServiceImpl implements FriendRequestService {

    @Autowired
    private FriendRequestRepository requestRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private SocketIoController socketIoController;
    @Override
    public int saveSendRequest(int senderId, int receiverId) {

        int status = 0;
        if(requestRepository.saveSendRequest(senderId,receiverId)==1){
            if(notificationRepository.saveNotificationOnSendFriendRequest(senderId,receiverId,"S")==1) {
                status = 1;
                // broadcast lastFriendRequest to server
                socketIoController.broadcastEvent("onAddFriend",requestRepository.getLastRequest(receiverId,false));
            }
        }
        return  status;

    }

    @Override
    public int confirmRequest(int senderId, int receiverId) {
        int status = 0;
        if(requestRepository.confirmRequest(senderId,receiverId)==1){
            // Save to mate_list
            if(requestRepository.saveMate(receiverId,senderId)==1){

                if(notificationRepository.saveNotificationOnConfirmFriend(receiverId,senderId,"A")==1) {
                    status = 1;
                    socketIoController.broadcastEvent("onConfirm",notificationRepository.getLastConfirmNotification(senderId,receiverId));
                }
            }
        }
        return  status;

    }

    @Override
    public int deleteRequest(int senderId, int receiverId) {
        return requestRepository.deleteFriendRequest(senderId,receiverId);
    }

    @Override
    public int removeFriend(int userId, int mateId) {
        return requestRepository.removeFriend(userId,mateId);

    }

    @Override
    public Boolean checkFriend(int userId, int mateId) {
        return requestRepository.checkFriend(userId,mateId);
    }

    @Override
    public List<FriendRequest> getAllFriendRequest(int reciverId, Boolean confirm) {
        System.out.println(requestRepository.getAllRequest(reciverId,confirm));
        return requestRepository.getAllRequest(reciverId,confirm);
    }

    @Override
    public List<FriendRequest> getLastRequest(int reciverId, Boolean confirm) {
        return  requestRepository.getLastRequest(reciverId,confirm);
    }


}
