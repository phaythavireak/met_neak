package com.kshrd.services.serviceImpl;


import com.kshrd.models.Group;
import com.kshrd.models.School;
import com.kshrd.models.User;
import com.kshrd.models.from.FrmsocailMedia;
import com.kshrd.repositories.SchoolRepository;
import com.kshrd.repositories.UserGroupRepository;
import com.kshrd.repositories.UserRepository;
import com.kshrd.repositories.UserSchoolRepository;
import com.kshrd.services.UserGroupService;
import com.kshrd.services.UserService;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl  implements UserService {



    @PersistenceContext
    private EntityManager entityManager;
    private UserRepository userRepository;
    private SchoolRepository schoolRepository;
    private UserSchoolRepository userSchoolRepository;
    private UserGroupRepository userGroupRepository;
    private UserGroupService userGroupService;

    public UserServiceImpl(UserRepository userRepository, SchoolRepository schoolRepository, UserSchoolRepository userSchoolRepository, UserGroupRepository userGroupRepository, UserGroupService userGroupService) {
        this.userRepository = userRepository;
        this.schoolRepository = schoolRepository;
        this.userSchoolRepository = userSchoolRepository;
        this.userGroupRepository = userGroupRepository;
        this.userGroupService = userGroupService;
    }



    @Override
    public List<User> findMatesByUserId(int userId)
    {
        List<User> mate = new ArrayList<User>();
        try
        {
            mate = this.userRepository.findMatesByUserId(userId);
            if(mate != null)
            {
                for (int i = 0 ; i<mate.size();i++)
                {
                    mate.get(i).setSchools(schoolRepository.findSchoolByUserId1(mate.get(i).getId()));
                }
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return mate;
    }

    @Override
    public boolean saveuser(User user) {
        boolean status = false;
//        Group group = null;
        if (!checkUserByFacebookId(user.getFacebookId()))
        {

            if (userRepository.saveUser(user)==1)
            {

                status = true;
            }
        }
        return status;
    }

    @Override
    public List<User> findUserByFacebookId(String facebookId) {
        return this.userRepository.findUserByFacebookId(facebookId);

    }

    @Override
    public boolean checkUserByFacebookId(String facebookId) {
        List<User> user = userRepository.findUserByFacebookId(facebookId);
        System.out.println(user);
        if (user.size()>0)
        {
            return true;
        }
        return false;
    }


    @Override
    public List<User> getall() {
        return this.userRepository.getall();
    }

    @Override
    public List<User> findAllUserFriendByUserId(int userId) {
        return this.userRepository.findAllUserFriendByUserId(userId);
    }

    @Override
    public List<User> findAllUserInGroup(int groupId) {
        return this.userRepository.findAllUserInGroup(groupId);
    }

    @Override
    public List<User> findUserById(int userId) {
        return this.userRepository.findUserById(userId);
    }

    @Override
    public List<User> findUserbyEmail(String email) {
        return this.userRepository.findAllByEmail(email);
    }

    @Override
    public boolean saveUserSocailmedia(FrmsocailMedia frmsocailMedia) {
        if (userRepository.saveUserSocailmedia(frmsocailMedia) == 1)
        {
            return true;
        }
        return false;
    }


    @Override
    public int countAllUserFriend(int user_id) {
        return this.userRepository.countAllUserFriend(user_id);
    }

    @Override
    public int countAllUserInGroup(int groupId) {
        return this.userRepository.countAllUserInGroup(groupId);
    }

    @Override
    public int countAllUser() {
        return this.userRepository.countAllUser();

    }

    @Override
    public boolean updateUserPhoto(String profileUrl, String coverUrl, int userId) {
        if (userRepository.updateUserPhoto(profileUrl,coverUrl,userId) == 1)
        {
            return true;
        }
        return false;
    }
    @Override
    public int countNotificationByTargetUserId(int target_user_id) {
        return this.userRepository.countNotificationByTargetUserId(target_user_id);
    }

    @Override
    public int countNotificationByTargetGroupId(int userId) {
        return this.userRepository.countAllUserInGroup(userId);
    }

    @Override
    public int countAllUserBySchoolAndLevel(String school, String level) {
        return this.userRepository.countAllUserBySchoolAndLevel(school,level);
    }

    @Override
    public int updateIsReadNotification(int userId) {
        return this.userRepository.updateIsReadNotification(userId);
    }


    @Override
    public List insertUpdateRemember(int userId, int mateId, boolean status) {
        StoredProcedureQuery query =  entityManager.createStoredProcedureQuery("InsertUpdateUserAndMate");

        query.registerStoredProcedureParameter(1,Integer.class,ParameterMode.IN);
        query.registerStoredProcedureParameter(2,Integer.class,ParameterMode.IN);
        query.registerStoredProcedureParameter(3,Boolean.class,ParameterMode.IN);

        query.setParameter(1,userId);
        query.setParameter(2,mateId);
        query.setParameter(3,status);
        return query.getResultList();

    }


    @Override
    public List<String> checkUserReadNotification(int userId, int notificationId) {
        StoredProcedureQuery query =  entityManager.createStoredProcedureQuery("checkuserreadnotification");

        query.registerStoredProcedureParameter(1,Integer.class,ParameterMode.IN);
        query.registerStoredProcedureParameter(2,Integer.class,ParameterMode.IN);

        query.setParameter(1,userId);
        query.setParameter(2,notificationId);
        return query.getResultList();
    }

    @Override
    public int checkuserandmatebyid(int userId, int mateId) {
        StoredProcedureQuery query =  entityManager.createStoredProcedureQuery("checkuserandmatebyid");
        query.registerStoredProcedureParameter(1,Integer.class,ParameterMode.IN);
        query.registerStoredProcedureParameter(2,Integer.class,ParameterMode.IN);
        query.setParameter(1,userId);
        query.setParameter(2,mateId);
//        return query.getResultList();
        Integer count = (Integer) query.getSingleResult();
        return count;
    }


    @Override
    public List<String> insertuserreadnotification(int notificationId, int userId) {
        StoredProcedureQuery query =  entityManager.createStoredProcedureQuery("insertuserreadnotification");
        query.registerStoredProcedureParameter(1,Integer.class,ParameterMode.IN);
        query.registerStoredProcedureParameter(2,Integer.class,ParameterMode.IN);
        query.setParameter(1,notificationId);
        query.setParameter(2,userId);
        return query.getResultList();
    }

    @Override
    public List<User> findAllUserBySchoolAndLevel(String school, String level) {
        return this.userRepository.findAllUserBySchoolAndLevel(school,level);
    }

    @Override
    public boolean updateUserById(User user) {
        boolean status = false;
        if(userRepository.updateUserById(user) == 1)
        {
            System.out.println("Update User");
            if (userSchoolRepository.deleteUserSchoolByUserId1(user.getId()) == 1) {
                System.out.println("Delete");
                for (School s: user.getSchools()) {
                    if (schoolRepository.saveUserSchools(s.getId(),user.getId())==1)
                    {
                        System.out.println("After delete -> Save" + user);
                        userGroupRepository.deleteUserGroup(user.getId());
                        status = userGroupService.addUserToGroup(user);
                    }
                }

            }

        }
        return status;
    }

    @Override
    public boolean registerUser(User user) {
        boolean status = false;
        if (userRepository.registerUser(user) == 1)
        {
            System.out.println("Successfully save user!");
            for (School s: user.getSchools()) {
                System.out.println(s);
                if (schoolRepository.saveUserSchools(s.getId(),user.getId())==1)
                {
                    System.out.println("Successfully save user school!");

                    System.out.println("USER: " + user);
                }
            }
            if (userGroupService.addUserToGroup(user))
            {
                System.out.println("Successfully add user to group!");
                status = true;
            }

        }
        return status;
    }


    //==================================================================================================================================================================


































}

