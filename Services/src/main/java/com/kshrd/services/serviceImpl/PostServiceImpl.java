package com.kshrd.services.serviceImpl;

import com.kshrd.controller.SocketIoController;
import com.kshrd.models.Group;
import com.kshrd.models.Post;
import com.kshrd.models.User;
import com.kshrd.repositories.GroupRepository;
import com.kshrd.repositories.NotificationRepository;
import com.kshrd.repositories.PostRepository;
import com.kshrd.services.PostService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;

    private NotificationRepository notificationRepository;

    private SocketIoController socketIoController;

    private Logger logger = LogManager.getLogger();

    @Autowired
    public void setPostRepository(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Autowired
    public void setNotificationRepository(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @Autowired
    public void setSocketIoController(SocketIoController socketIoController) {
        this.socketIoController = socketIoController;
    }

    /**
     * 1. savePostInGroup
     */

    @Autowired
    GroupRepository groupRepository;

    @Override
    public Integer savePostInGroup(Post post) {
        Integer save = postRepository.savePostInGroup(post);
        if (save > 0) {
            // create notification
            //first, find current post_id
            Integer currentPostId = postRepository.getCurrentPostId();

            //second, group_id
            // find group with name by id
            Integer groupId = post.getgId();

            //get group
            Group group=groupRepository.findGroupById(groupId);

            //get user id post
            Integer userId = post.getuId();
            notificationRepository.saveNotificationForGroupPost(userId,group,currentPostId,"G");
//            notificationRepository.saveNotificationForGroupPost();

            // broadcast post in group
            Post lastPost = postRepository.getLastPost();
            socketIoController.broadcastEvent("onPostGroup",lastPost);
        }

        return save;
    }


    /**
     * 2. findPostsByGroupId
     */
    @Override
    public Page<Post> findPostsByGroupId(Integer groupId, Pageable pageable) {
        return postRepository.findPostsByGroupId(groupId, pageable);
    }


    /**
     * 3. findPostsByAllGroupId
     */
    @Override
    public Page<Post> findPostsByAllGroupId(Integer g1, Integer g2, Integer g3, Pageable pageable) {
        return postRepository.findPostsByAllGroupId(g1, g2, g3, pageable);
    }


    /**
     * 4. countPostByGroupId
     */
    @Override
    public Integer countPostByGroupId(Integer g, Boolean status) {
        return postRepository.countPostByGroupIdAndStatus(g, status);
    }

    /**
     * 5. countPostByAllGroupId
     */
    @Override
    public Integer countPostByAllGroup(Integer g1, Integer g2, Integer g3) {
        return postRepository.countPostByAllGroup(g1, g2, g3);
    }


    /**
     * 6. findAllPosts
     */
    @Override
    public List<Post> findAllPost() {
        List<Post> allPost = postRepository.findAllPost();
        return allPost;
    }

    /**
     * 7. findPostsByUserId
     */
    @Override
    public Page<Post> findPostByUserId(Integer uid, Pageable pageable) {
        return postRepository.findPostByUserId(uid, pageable);
    }


    /**
     * 8. countPostsByUserId
     */
    @Override
    public Integer countPostByUserId(Integer uid) {
        return postRepository.countPostByUserId(uid);
    }

    /**
     * 9. findPostById
     */
    @Override
    public Post findPostById(Integer id, Boolean status) {
        return postRepository.findPostByIdAndStatus(id, status);
    }

    /**
     * 10. filterPostsByGroupId Date and status
     */
    @Override
    public Page<Post> filterPostsByGroupId(Integer gId, String startDate, String endDate, Boolean status, Pageable pageable) {
        return postRepository.filterPostsByGroupId(gId, startDate, endDate, status, pageable);
    }


    /**
     * 11. findPostsByMateId
     */
    @Override
    public Page<Post> findPostByMateId(Integer mid, Boolean status, Pageable pageable) {
        return postRepository.findPostByMateIdAndStatus(mid, status, pageable);
    }

    /**
     * 11. save Post  for mate
     */
    @Override
    public Integer savePostForMate(Post post) {
        Integer save = postRepository.savePostFormMate(post);
        if (save > 0) {
            // get current post
            Integer currentPostId = postRepository.getCurrentPostId();
            // create notification
            notificationRepository.saveNotificationForMatePost(post.getuId(), post.getMateId(), currentPostId, "W");

            // broadcast post in mate
            Post lastPost = postRepository.getLastPost();
            socketIoController.broadcastEvent("onPostMate",lastPost);
        }
        return save;
    }

    /**
     * delete Post by Id
     */
    @Override
    public Integer deletePostById(Integer id) {
        socketIoController.broadcastEvent("onDeletePost",findPostById(id,true));
        return postRepository.deletePostById(id);
    }

    /**
     * update Post by Id
     */
    @Override
    public Integer updatePostById(Post post) {
        socketIoController.broadcastEvent("onEditePost",findPostById(post.getId(),true));
        return postRepository.updatePostById(post);
    }


}
