package com.kshrd.services.serviceImpl;


import com.kshrd.models.Group;
import com.kshrd.models.User;
import com.kshrd.repositories.GroupRepository;
import com.kshrd.repositories.UserGroupRepository;
import com.kshrd.services.NotificationService;
import com.kshrd.services.UserGroupService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

@Service
public class UserGroupServiceImpl implements UserGroupService {


    @PersistenceContext
    private EntityManager entityManager;
    private UserGroupRepository userGroupRepository;
    private GroupRepository groupRepository;
    private NotificationService notificationService;

    public UserGroupServiceImpl(UserGroupRepository userGroupRepository, GroupRepository groupRepository, NotificationService notificationService) {
        this.userGroupRepository = userGroupRepository;
        this.groupRepository = groupRepository;
        this.notificationService = notificationService;
    }


    @Override
    public boolean addUserToGroup(User user) {
        Group group;
        boolean status = false;

        for (int i = 0; i < user.getSchools().size(); i++) {
            String name = user.getSchools().get(i).getName();
            String level = user.getSchools().get(i).getLevel();
//            String y = user.getUserSchools().get(i).getGraduatedYear() + "";
//            int gYear = Integer.parseInt(y.split("-")[0]);
            int gYear = 2010;


            if (checkGroupIfExist(name, level, gYear)) {
                System.out.println("true Value");
                System.out.println("Successfully Check group");
                Group groups = groupRepository.findGroupBySchool(name, level, gYear);
//                for (Group g : groups)
//                {
                if (userGroupRepository.addUserToGroup(groups.getId(), user.getId()) == 1) {
                    System.out.println("Successfully add user to group");
                    if (notificationService.saveNotificationWhenUserJoinGroup(user.getId(), groups)) {
                        System.out.println("Successfully save notification");
                        status = true;
                    }
                }
//                }


            } else {


                System.out.println("false Value");
                group = new Group();
//                y = user.getUserSchools().get(i).getGraduatedYear() + "";
//                gYear = Integer.parseInt(y.split("-")[0]);
                group.setName(user.getSchools().get(i).getName() + "," + user.getSchools().get(i).getLevel() + "," + 2010);
                Group group1 = groupRepository.save(group);
                if (group1.getId() != null) {
                    System.out.println("Successfully create group");
                    if (userGroupRepository.addUserToGroup(group.getId(), user.getId()) == 1) {
                        System.out.println("Successfully add user to group");
                        if (notificationService.saveNotificationWhenUserJoinGroup(user.getId(), group)) {
                            System.out.println("Successfully save notification");
                            status = true;
                        }
                    }
                }
            }
        }
        return true;

    }


    @Override
    public boolean checkGroupIfExist(String name, String level, int year) {
        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("checkgroup");
        query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN);
        query.setParameter(1, name);
        query.setParameter(2, level);
        query.setParameter(3, year);
        Boolean result = (Boolean) query.getSingleResult();
        return result;
    }
}



