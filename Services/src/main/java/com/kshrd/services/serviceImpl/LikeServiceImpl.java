package com.kshrd.services.serviceImpl;


import com.kshrd.controller.SocketIoController;
import com.kshrd.models.Like;
import com.kshrd.repositories.LikeRepository;
import com.kshrd.repositories.NotificationRepository;
import com.kshrd.repositories.PostRepository;
import com.kshrd.services.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    private LikeRepository likeRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private NotificationRepository notificationRepository;

    private SocketIoController socketIoController;

    @Autowired
    public void setSocketIoController(SocketIoController socketIoController) {
        this.socketIoController = socketIoController;
    }

    @Override
    public int deleteLike(int userId, int postId) {

        return likeRepository.deleteLike(userId,postId);

    }

    @Override
    public int saveLike(int userId, int postId) {
        int status = 0;

        if(likeRepository.findLike(userId,postId)==0){
            System.out.println(likeRepository.findLike(userId,postId)+ "likeRepository.findLike(userId,postId)");
            if(likeRepository.saveLike(userId,postId)==1)
                 {
                     notificationRepository.saveNotificationOnUserLike(userId,postId,"L");
                     System.out.println("save like");
                     status=1;

                     // broadcast like to server by post
                     Integer totalLike=likeRepository.countLike(postId);
                     socketIoController.broadcastEvent("onLike",totalLike);

                }
            }
            else {
            System.out.println("User like Post already !!!!");
        }

        return  status;
    }

    @Override
    public int countLike(int postId) {
        System.out.println(likeRepository.countLike(postId));
        return likeRepository.countLike(postId);
    }

    @Override
    public int countLikesByUserId(int userId) {
        return likeRepository.countLikesByUserId(userId);
    }

    @Override
    public List<Like> getAllLikes(int postId, Boolean status) {
        return likeRepository.findLikeByPostIdAndStatus(postId,status);
    }

}
