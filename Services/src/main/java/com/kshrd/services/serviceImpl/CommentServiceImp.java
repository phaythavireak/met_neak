package com.kshrd.services.serviceImpl;


import com.kshrd.controller.SocketIoController;
import com.kshrd.models.Comment;
import com.kshrd.models.Post;
import com.kshrd.repositories.CommentRepository;
import com.kshrd.repositories.NotificationRepository;
import com.kshrd.repositories.PostRepository;
import com.kshrd.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImp implements CommentService {

    private CommentRepository commentRepository;

    private NotificationRepository notificationRepository;

    private PostRepository postRepository;

    private SocketIoController socketIoController;

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Autowired
    public void setNotificationRepository(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @Autowired
    public void setPostRepository(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Autowired
    public void setSocketIoController(SocketIoController socketIoController) {
        this.socketIoController = socketIoController;
    }

    /**
     * find comment by post id with pagination
     *
     * @param id
     * @return
     */
    @Override
    public Page<Comment> findCommentByPostId(Integer id, Boolean status, Pageable pageable) {
        return commentRepository.findCommentByPostIdAndStatus(id, status, pageable);
    }

    /**
     * find one comment by id
     *
     * @param id
     * @return
     */
    @Override
    public Comment findCommentById(Integer id, Boolean status) {
        return commentRepository.findCommentByIdAndStatus(id, status);
    }


    /**
     * countCommentByPostId
     *
     * @param id
     * @return
     */
    @Override
    public Integer countCommentByPostId(Integer id, Boolean status) {
        return commentRepository.countCommentByPostIdAndStatus(id, status);
    }

    /**
     * findCommentByUserId
     *
     * @param id
     * @param pageable
     * @return
     */
    @Override
    public Page<Comment> findCommentByUserId(Integer id, Boolean status, Pageable pageable) {
        return commentRepository.findCommentByUserIdAndStatus(id, status, pageable);
    }

    /**
     * countCommentByUserId
     *
     * @param id
     * @return
     */
    @Override
    public Integer countCommentByUserId(Integer id, Boolean status) {
        return commentRepository.countCommentByUserIdAndStatus(id, status);
    }

    private static Integer postId;
    private static Integer cId;
    private static Integer userOwnPostId;
    private static List<Integer> allUserComments = new ArrayList<>();

    //saveComment
    @Override
    public Integer saveComment(Comment comment) {
        Integer saveId = commentRepository.saveComment(comment);
        if (saveId > 0) {

            //broadcast comment to client
            Comment lastComment=commentRepository.getLastComment();
            socketIoController.broadcastEvent("onComment", comment);

            // current comment id
            cId = commentRepository.getCurrentComment();

            // get post to check post you have comments
            Post post = postRepository.findPostByIdAndStatus(comment.getpId(), true);

            //get owner posted
            userOwnPostId = post.getUser().getId();

            postId = comment.getpId();

            // get all users comment on that post
            allUserComments = commentRepository.getUserIdCommentOnePostId(postId, comment.getUid());
            System.out.println("All ID: " + allUserComments);

            //meger all to list
//            allUserComments.add(userOwnPostId);
            if (post != null) {
                if (!allUserComments.isEmpty()) {
                    for (Integer targetUserId : allUserComments
                            ) {
                        if (notificationRepository.saveNotificationCommentPost(comment.getUid(), targetUserId, post.getId(), "C") > 1) {
                            //push to use notification
                        }
                    }
                } else {
                    notificationRepository.saveNotificationCommentPost(comment.getUid(), userOwnPostId, post.getId(), "C");
                }
            }
        }
        return saveId;
    }

    /**
     * updateCommentById
     *
     * @param comment
     * @return
     */
    @Override
    public Integer updateCommentById(Comment comment) {
        return commentRepository.updateCommentById(comment);
    }

    /**
     * deleteCommentById
     *
     * @param id
     * @return
     */
    @Override
    public Integer deleteCommentById(Integer id) {
        return commentRepository.deleteCommentById(id);
    }
}
