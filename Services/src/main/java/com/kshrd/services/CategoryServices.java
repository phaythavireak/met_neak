package com.kshrd.services;

import com.kshrd.models.Category;
import com.kshrd.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServices {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> allCategory() {
        System.out.println("Runed serice");
        return categoryRepository.category();
    }
}
