package com.kshrd.services;

import com.kshrd.models.Comment;
import com.kshrd.models.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.sql.Timestamp;
import java.util.List;

public interface PostService {

    /**
     * 1. savePostInGroup by user
     */
    public Integer savePostInGroup(Post post);

    /**
     * 2. findPostsByGroupId
     */
    public Page<Post> findPostsByGroupId(Integer groupId, Pageable pageable);

    /**
     * 3. findPostsByAllGroupId
     */
    public Page<Post> findPostsByAllGroupId(Integer g1, Integer g2, Integer g3,Pageable pageable);

    /**
     * 4. countPostByGroupId
     */
    public Integer countPostByGroupId(Integer g,Boolean status);


    /**
     * 5. countPostByAllGroupId
     */
    public Integer countPostByAllGroup(Integer g1, Integer g2, Integer g3);

    /**
     * 6. findAllPosts
     */
    public List<Post> findAllPost();

    /**
     * 7. findPostsByUserId
     */
    public Page<Post> findPostByUserId(Integer uid, Pageable pageable);


    /**
     * 8. countPostsByUserId
     */
    public Integer countPostByUserId(Integer uid);


    /**
     * 9. findPostById
     */
    public Post findPostById(Integer id, Boolean status);

    /**
     * 10. filterPostsByGroupId Date and status
     */
    public Page<Post> filterPostsByGroupId(Integer gId, String startDate, String endDate,Boolean status,Pageable pageable);

    /**
     * 11. findPostsByMateId
     */
    public Page<Post> findPostByMateId(Integer mid, Boolean status,Pageable pageable);


    /**
     * 12 savePost for mate
     */
    public Integer savePostForMate(Post post);

    /**
     * delete Post by Id
     */
    public Integer deletePostById(Integer id);

    /**
     * update Post id
     */
    public Integer updatePostById(Post post);






}
