package com.kshrd.services;

import com.kshrd.models.Like;
import javafx.scene.control.Pagination;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LikeService {
    public int deleteLike(int userId, int postId);
    public int saveLike(int userId, int postId);
    public int countLike(int postId);
    public int countLikesByUserId(int userId);
    public List<Like> getAllLikes(int postId,Boolean status);
    //public List<Like> findLikesByUserId(int userId, Pagination pagination);
}
