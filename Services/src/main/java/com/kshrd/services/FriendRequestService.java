package com.kshrd.services;

import com.kshrd.models.FriendRequest;

import java.util.List;

public interface FriendRequestService {

    public int saveSendRequest(int senderId, int receiverId);
    public int confirmRequest(int senderId, int receiverId);
    public int deleteRequest(int senderId, int receiverId);

    public int removeFriend(int userId, int mateId);
    public Boolean checkFriend(int userId,int mateId);
    public List<FriendRequest> getAllFriendRequest(int reciverId,Boolean confirm);
    public List<FriendRequest> getLastRequest(int reciverId,Boolean confirm);
}
