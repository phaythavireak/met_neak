package com.kshrd.services;


import com.kshrd.models.Group;
import com.kshrd.models.Notification;
import com.kshrd.models.User;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationService {

    public int deleteLikeNotification( int userId, int postId, String type);
    public int saveNotificationCommentPost (int userId, int targetUserId, int postId, String type);
    public int saveNotificationOnUserLike(  int userId,  int postId,String type);
    public int saveNotificationOnConfirmFriend (int senderId, int receiverId,  String type);
    public int saveNotificationOnSendFriendRequest(  int senderId,   int receiverId, String type);
    Integer countNotificationFriendRequest(int currentUserId);

    List<User> getAcceptedRequest(int currentUserId);
    List<User> getAllAddFriendRequestNotification(int selfId);
    Integer getFriendRequestNotificationNumber(int currentUserId);
    public int updateTypeSendNotification(int currentUserId);
    public int updateTypeAcceptNotificaton(int currentUserId);

    //virak
    boolean saveNotificationWhenUserJoinGroup(int userId, Group group);
    List<Notification> getLastConfirmNotification(int target,int userAccept);
    //create by thon
    public Integer saveNotificationForGroupPost(int userId, Group group,int postId,String type);
    public Integer saveNotificationForMatePost(int userId, Integer mateId, int postId, String type);
}
