package com.kshrd.services;



import com.kshrd.models.User;

public interface UserGroupService {


    boolean addUserToGroup(User user);
    boolean checkGroupIfExist(String name, String level, int year);

}
