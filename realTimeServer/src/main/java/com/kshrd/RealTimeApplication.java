package com.kshrd;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class RealTimeApplication extends SpringBootServletInitializer implements ApplicationRunner {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {

        return builder.sources(RealTimeApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(RealTimeApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {



    }
}
