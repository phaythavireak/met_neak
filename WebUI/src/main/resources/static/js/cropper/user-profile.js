var croppedImage="";
function timeAgo(time) {

    switch (typeof time) {
        case 'number':
            break;
        case 'string':
            time = + new Date(time);
            break;
        case 'object':
            if (time.constructor === Date) time = time.getTime();
            break;
        default:
            time = + new Date();
    }
    var time_formats = [
        [60, 'seconds', 1], // 60
        [120, '1 minute ago', '1 minute from now'], // 60*2
        [3600, 'minutes', 60], // 60*60, 60
        [7200, '1 hour ago', '1 hour from now'], // 60*60*2
        [86400, 'hours', 3600], // 60*60*24, 60*60
        [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
        [604800, 'days', 86400], // 60*60*24*7, 60*60*24
        [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
        [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
        [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
        [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
        [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
        [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
        [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
        [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    var seconds = ( + new Date() - time) / 1000,
        token = 'ago',
        list_choice = 1;

    if (seconds == 0) {
        return 'Just now';
    }
    if (seconds < 0) {
        seconds = Math.abs(seconds);
        token = 'from now';
        list_choice = 2;
    }
    var i = 0,
        format;
    while (format = time_formats[i++])
        if (seconds < format[0]) {
            if (typeof format[2] == 'string')
                return format[list_choice];
            else
                return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
        }
    return time;
}
function croppedSucess(image){
   croppedImage = image;
}
//Find post by postId
function findPostByPostId (postId) {
    page=1;
    $.ajax({
        url: "/v1/api/post/"+postId,
        type: "GET",
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
        },
        success: function (data) {

            if(page <= 2) {
                $("#popPost").empty();
            }
            // $("#popPost").append('<div class="row"> ' +
            //     '   <div class="col-md-3 col-sm-3"> ' +
            //     '       <a href=/user-profile?userId=' + data.data.user.id + '>' +
            //     '       <div class="user-profile-pic"> ' +
            //     '           <img width="100px" height="100px" data-src-retina="images/img/profiles/avatar2x.jpg" data-src="images/img/profiles/avatar.jpg" src="' + data.data.photoUrl + '" alt=""> ' +
            //     '           <h4 class="semi-bold">' + data.data.user.firstName + ' ' + data.data.user.lastName + '</h4> ' +
            //     '       </div> ' +
            //     '       </a>' +
            //     '       <div class="user-mini-description"></div> ' +
            //     '   </div> ' +
            //     '   <div class="col-md-8 user-description-box  col-sm-5"> ' +
            //     '       <p><i class="fa fa-graduation-cap"></i>' + data.data.user.schools + '</p> ' +
            //     '       <p><i class="fa fa-user"></i><b>ភេទ </b>' + data.data.user.gender + '</p> ' +
            //     '       <p><i class="fa fa-user"></i><b>ភេទ </b>' + data.data.user.gender + '</p> ' +
            //     '       <p><i class="fa fa-map-marker"></i><b>ទីកន្លែងកំណើត</b> <b>ក្រុង/ខេត្ត</b> ' + data.data.location + ' <b>ស្រុក</b> ' + data.location + '</p> ' +
            //     //'       <p><i class="fa fa-map-marker"></i><b>ទីកន្លែងកំណើត</b> ក្រុង/ខេត្ត Test <b>ស្រុក</b> Test </p> ' +
            //     '       <p><i class="fa fa-envelope"></i><b>ថ្ងៃខែឆ្នាំកំណើត</b> ' + data.data.user.dob + ' </p> ' +
            //     '   </div> ' +
            //     '   <div class="clearfix"></div>' +
            //     '</div>');
            console.log(data.data.user.id);
            var postTime = timeAgo(data.data.createdDate);
            if(postTime == "Yesterday") {
                var d = new Date(data.data.createdDate);
                postTime += " at " + (d.getHours() > 12 ? d.getHours() - 12 + ":" + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes() + " PM" : d.getHours() + ":" + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes() + " AM");
            }
            $("#popPost").append(
                '<div class="row " style=" width: 100%;margin: auto;">' +
                '<div class="col-sm-8" style="padding-bottom: 10px;padding-top: 10px;">' +
                '<img style="width: 100%;" src="'+data.data.photoUrl+'">' +
                '</div>' +
                '   <div class="col-sm-4 img-alert" style="padding-top: 15px;margin-left: -15px">' +
                '<div class="name-pro">'+
                '<div class="col-md-2">' +
                '<a href="/user-profile?userId='+data.data.user.id+'">'+
                '<img style="width: 35px;height: 35px" src="'+data.data.user.profilePhotoUrl+'">' +
                '</a>'+
                '</div>' +
                '<div class="col-md-10 name-cmt" style="margin-top: -10px">' +
                '<h5>'+data.data.user.firstName+" "+data.data.user.lastName+'</h5>' +
                '<div class="muted" style="margin-top: -5px">'+postTime+'</div>'+
                '</div>'+
                '</div>' +
                '<div style="margin-left: -15px; margin:10px 0px 10px 0px ;" class="col-md-12">' +
                data.data.content+
                '</div>' +
                '<div class="col-md-12">' +
                "<hr style='margin-top: 0;margin-bottom: 0'/ >"+
                "    <div class='tiles padding-10' style='background-color: white;  height:30px;'>" +
                "       <div class='pull-left'>" +
                "           <a href='javascript:void(0);' onclick='likePost("+data.data.id+")' data-heart=\"0\"><img src=\"/images/icon/heart.png\" id='mn-like-post"+data.data.id+"' style=\"width :15px; margin-top: -15px\"></a>" +
                "        </div>" +
                "        <div class=\"pull-right\">" +
                "            <a href=\"javascript:void(0);\" id=\"comment-post\" onclick='commentPost("+data.data.id+")' ><img src=\"/images/icon/comment.png\" style=\"width :15px; margin-top: -15px\"></a>" +
                "        </div>" +
                "        <div class=\"clearfix\"></div>" +
                "    </div>" +
                "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                "    <div class=\"tiles grey padding-10\" id='mn-like-container"+data.data.id+"' style =\"background-color: white; height:32px;\">" +
                "        <div class=\"clearfix\"></div>" +
                "    </div>" +
                // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                "    <div class=\"tiles grey padding-10\" style=\"background-color: white;  height: 40px;  \">" +
                "       <div>" +
                "          <input type=\"text\" id='mn-comment"+data.data.id+"' placeholder=\"បញ្ចេញមតិរបស់អ្នក......\" style=' position: relative; width:100% ;margin-top: -9px'  onkeydown = \"if (event.keyCode == 13)\n" +
                "                        document.getElementById('myBtn"+data.data.id+"').click()\"> "+
                "               <button hidden='hidden' onclick='btnClick("+data.data.id+")' id='myBtn"+data.data.id+"'>Button</button>"+
                "        </div>" +
                "        <div class=\"clearfix\"></div>" +
                "    </div>" +
                // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                "    <div class=\"tiles padding-10\" id='mn-comment-form' style='background-color: white; marginn-top:0px\'>" +
                "<input type='hidden' id='cmt-page"+data.data.id+"'/>"+
                "<input value='1' type='hidden' id='prev-page"+data.data.id+"'/>"+
                "<div id='previous-comment" + data.data.id + "' style='display: none'>"+
                "       <a href='javascript:void(0)' onclick='previousComment("+data.data.id+")'>មើលការផ្ដល់មតិពីមុន</a> " +
                "</div>"+
                "       <div class=\"pull-left\">" +
                "           <ul id='comment-container"+data.data.id+"' style='height: 355px;overflow-y: auto;'>"+
                // "                <li>"+
                // "                   <a href=\"https://www.w3schools.com\">" +
                // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                // "                   </a>" +
                // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                // "                </li>"+
                // "                <li>"+
                // "                   <a href=\"https://www.w3schools.com\">" +
                // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                // "                   </a>" +
                // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                // "                </li>"+
                "           </ul>"+
                // "          <a href=\"https://www.w3schools.com\"><img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish</a><p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                "        </div>" +
                '</div>' +
                '</div>'+
                '</div>' +
                '</div>');
            getAllCommentByPostId(data.data.id);
            countLike(data.data.id);
            page=2;
        },
        error: function () {

        }
    });//ajax
}
function checkLike(postId) {
    $.ajax({
        url: "/v1/api/like?postId=" +postId,
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
        },
        success: function (data) {
            for (var i = 0; i<data.data.length;i++){
                if(data.data[i].user.id==uid){
                    document.getElementById("mn-like-post"+postId).src="images/icon/heartYellow.png";
                }
                else{
                    document.getElementById("mn-like-post"+postId).src="images/icon/heart.png";
                }
            }
        },
        error: function (err) {
            swal("មិនដំណើរការ", err.message, "error");
        }
    });
    document.getElementById("mn-like-post"+postId).src="images/icon/heart.png";
}
function countLike(postId){
    $.ajax({
        url: "/v1/api/like/count/"+ postId,
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
        },
        success: function (data) {
            if(data>0){
                $("#mn-like-container"+postId).empty();
                $("#mn-like-container"+postId).append(
                    '<div class="pull-left">' +
                    '<img src="images/icon/heartYellow.png" style ="width:14px; margin-top: -13px">'+
                    '<p id="p" style ="color: black; font-size: 12px">&nbsp;&nbsp;'+data+'</p>' +
                    '</div>'
                );
                $("#mn-like-container"+postId).css("display","")
            }
            else{
                $("#mn-like-container"+postId).css("display","none");
            }
            checkLike(postId);
        },
        error: function (err) {
            swal("មិនដំណើរការ", err.message, "error");
        }
    }); //count like and append
}
function likePost(postId){
    $.ajax({
        url: "/v1/api/like/" + postId,
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
        },
        success: function (data) {
            countLike(postId);
        },
        error: function (err) {
            swal("មិនដំណើរការ", err.message, "error");
        }
    }); //Add like
}
function commentPost(postId){
    $("#mn-comment"+postId).focus();
};
function getAllCommentByPostId(postId){
    //find total page of comment in a post
    $.ajax({
        url: "/v1/api/comment/count/" + postId,
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
        },
        success: function (data) {
            var page = Math.ceil(data/5);
            if(page!=0){
                $.ajax({
                    url: "/v1/api/comment?postId=" + postId+"&page="+page,
                    type: "GET",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                    },
                    success: function (data) {
                        $("#cmt-page"+postId).val(data.pagination.total_pages);
                        var totalPage = data.pagination.total_pages;
                        $("#comment-container"+postId).empty();
                        for (var i = 0; i < data.data.length; i++) {
                            $("#comment-container"+postId).append(
                                "<li>" +
                                "   <a href='/user-profile?userId="+data.data[i].user.id+"'>" +
                                "       <img src='"+data.data[i].user.profilePhotoUrl+"' style='width: 25px'>" +
                                "       <span>"+data.data[i].user.firstName+" "+data.data[i].user.lastName+"</span>"+
                                "   </a>" +
                                "<p style ='color: black;font-size: 13px; padding-top: 3px;margin-left: -8px'>&nbsp;&nbsp;"+data.data[i].comment+"</p>" +
                                "</li>"
                            );
                        }
                        if(totalPage>1){
                            $("#previous-comment"+postId).css("display","")
                        }
                    },
                    error: function (err) {
                        swal("មិនដំណើរការ", err.message, "error");
                    }
                })
            }
            // console.log(cmtpage)
        },
        error: function (err) {
            swal("មិនដំណើរការ", err.message, "error");
        }
    })
}
function getPreviousComment(postId,page){
    //find total page of comment in a post
    if(page>0){
        $.ajax({
            url: "/v1/api/comment/desc?postId=" + postId+"&page="+page,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
            },
            success: function (data) {
                for (var i = 0; i < data.data.length; i++) {
                    $("#comment-container"+postId).prepend(
                        "<li>" +
                        "   <a href='/user-profile?userId="+data.data[i].user.id+"'>" +
                        "       <img src='"+data.data[i].user.profilePhotoUrl+"' style='width: 25px'>" +
                        "       <span>"+data.data[i].user.firstName+" "+data.data[i].user.lastName+"</span>"+
                        "   </a>" +
                        "<p style ='color: black;font-size: 13px; padding-top: 3px;margin-left: -8px'>&nbsp;&nbsp;"+data.data[i].comment+"</p>" +
                        "</li>"
                    );
                }
            },
            error: function (err) {
                swal("មិនដំណើរការ", err.message, "error");
            }
        })
    }
    // if(page==1){
    //     $("#previous-comment"+postId).css("display","none")
    // }
}
function previousComment(postId) {
    var cmtPage =$("#cmt-page"+postId).val();
    var prevPage = $("#prev-page"+postId).val();
    if(prevPage==1){
        $("#comment-container"+postId).empty();
    }
    getPreviousComment(postId,prevPage);
    prevPage++;
    $("#prev-page"+postId).val(prevPage);
    if(prevPage>cmtPage){
        $("#previous-comment"+postId).css("display","none")
    }
}
function btnClick(postId) {
    document.getElementById("mn-comment-form").style.display ="block";
    //document.getElementById("mn-comment").setAttribute("value","");
    var comment = $('#mn-comment'+postId).val();
    if(comment!=''){
        var postData={
            comment:comment
        }
        $.ajax({
            url: "/v1/api/comment/" + postId,
            type: "POST",
            data: JSON.stringify(postData),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
            },
            success: function (data) {
                //Append comment to form by specific postId
                $('#mn-comment'+postId).val("");
                $("#mn-comment"+postId).blur();
                $("#comment-container"+postId).append(
                    "<li>" +
                    "   <a href='/user-profile?userId="+data.data[0].user.id+"'>" +
                    "       <img src='"+data.data[0].user.profilePhotoUrl+"' style='width: 25px'>" +
                    "       <span>"+data.data[0].user.userName+"</span>"+
                    "   </a>" +
                    "   <p style ='color: black;font-size: 13px;'>&nbsp;&nbsp;"+data.data[0].comment+"</p>" +
                    "</li>"
                );
                //For user mention notification Save
                var mentionUserId =  $("#mentionUId"+postId).val();
                if (mentionUserId){
                    ///////Save notification here
                    $.ajax({
                        url: "/v1/api/notification/saveMentionNotification?userId="+uid+"&tUserId="+mentionUserId+"&postId="+postId,
                        type: "POST",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                            xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                        },
                        success: function (data) {
                            console.log("Success save mention notification");
                        },
                        error: function (err) {
                            swal("មិនដំណើរការ", err.message, "error");
                        }
                    }); //Add comment
                }
            },
            error: function (err) {
                swal("មិនដំណើរការ", err.message, "error");
            }
        }); //Add comment
    }
    else{
        $("#mn-comment"+postId).focus();
    }
}
$(document).ready(function () {
    var pageGall = 2;
    var myStatus;
    var page = "1";
    var canScroll = true;
    var checkSession = function () {
        if(getJSessionId() == null || getJSessionId() == "") {
            window.location.href = "/";
        }
    }
    function getJSessionId(){
        var jsId = document.cookie.match(/JSESSIONID=[^;]+/);
        if(jsId != null) {
            if (jsId instanceof Array)
                jsId = jsId[0].substring(11);
            else
                jsId = jsId.substring(11);
        }
        return jsId;
    }
    function validateImage(file) {
        var ext = file.name.split(".");
        ext = ext[ext.length-1].toLowerCase();
        var arrayExtensions = ["jpg" , "jpeg", "png", "bmp", "gif"];

        var isValidSize = false;
        var imageSize= file.size/1000;
        if(imageSize <= 5000) {
            isValidSize = true;
        }

        if (arrayExtensions.lastIndexOf(ext) == -1) {
            swal("", "ប្រភេទរូបភាពដែលអ្នកបានជ្រើសរើសមិនត្រឺមត្រូវទេ!", "error")
            return false;
        }
        if (isValidSize == false) {
            swal("", "រូបភាពដែលបានជ្រើសរើសលើសទំហំ​ អនុញ្ញាតត្រឹមតែ៥MB ប៉ុណ្ណោះ", "error")
            return false;
        }
        return true;
    }
    $(document).on("click", "#remeberedMate", function () {
        //alert("senderId : "+uid +"  receiverId : "+mateId);
        checkSession();
        var text, text1;
        if(myStatus == false) {
            text = "តើអ្នកពិតជាចង់លុបគាត់ចេញពីបញ្ចីមិត្តដែរអ្នកបានចង់ចាំមែនរឺ?";
            text1 = "ការលុបបានជោគជ័យ!"

        }
        else {
            text = "តើអ្នកពិតជាចង់ដាក់គាត់ចូលក្នុងបញ្ចីមិត្តដែរអ្នកបានចង់ចាំមែនរឺ?";
            text1 = "ការបញ្ចូលបានសំរេច!"
        }
       //////////////////
        swal({
            title :  "មិត្តភ័ក្ត",
            text : text,
            icon : "info",
            buttons: [
                'No',
                'Yes'
            ],
        }).then((isConfirm) => {

            if(isConfirm){
                saveSendFriendRequest(mateId);
                $.ajax({
                    url: "/v1/api/user/remember/"+uid+"/"+mateId+"/"+myStatus,
                    type: "GET",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                    },
                    success: function (data) {
                        swal(text1);

                        $("#remeberedMatemn-user-timeline").empty();
                        if(myStatus == false)
                        {
                            myStatus = true;
                            $("#remeberedMate").empty();
                            $("#remeberedMate").append('<i class=""></i>&nbsp;&nbsp;ស្គាល់');
                        }
                        else{
                            myStatus = false;
                            $("#remeberedMate").empty();
                            $("#remeberedMate").append('<i class="fa fa-check"></i>&nbsp;&nbsp;បានស្គាល់');
                        }

                    },
                    error: function (err) {
                        swal("មិនដំណើរការ", err.message, "error");
                    }
                });
            }
        });
    });
    var findMateByMateId = function (userId) {
        $.ajax({
            url: "/v1/api/user/" + userId,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
            },
            success: function (data) {
                data = data.data;
                var dob = new Date(data.dob);
                var username = data.firstName + ' ' + data.lastName;
                var gender = data.gender;
                var pob = data.location;
                var profilepic = data.profilePhotoUrl;
                var school = '';
                var monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                ];
                var coverUrl;
                var graduateYear = data.schools[0].graduatedYear;
                console.log(graduateYear);
                // var chkMyProfile = true;

                pob = '<b> ខេត្ត/ក្រុង </b>' + pob.provinceName + '<b> ស្រុក/ខ័ណ្ឌ </b>' + pob.districtName;
                dob = '' + ("0" + dob.getDate()).slice(-2) + '-' + monthNames[dob.getMonth()] + '-' + dob.getFullYear();
                for (var i = 0; i<data.schools.length; i++) {
                    var schoolId = data.schools[i].id;
                    var schoolName = data.schools[i].name;
                    var schoolLevel = data.schools[i].level;
                    if (schoolLevel == 'h'){
                        schoolLevel = 'វិទ្យាល័យ ';
                    } else if(schoolLevel == 's'){
                        schoolLevel = 'អនុវិទ្យាល័យ ';
                    } else {
                        schoolLevel = 'បឋមសិក្សា '
                    }
                    school += schoolLevel + schoolName + ' ' ;
                }
                if (gender == 'M'){
                    gender = 'ប្រុស';
                } else {
                    gender = 'ស្រី';
                }
                if (data.coverPhotoUrl == "" || data.coverPhotoUrl == null) {
                    coverUrl = "images/img/cover-pic-2.png"
                } else {
                    coverUrl = data.coverPhotoUrl;
                }
                $("#mn-user-timeline").empty();
                if (uid == mateId){ // check if it's a current user
                    //console.log("OwnID ="+uid);
                    //console.log("MateID ="+mateId);
                    $("#page-title").text(username);
                    $("#mn-user-timeline").append(
                        '<div class="tiles green cover-pic-wrapper">'+
                        '<div class="overlayer top-right" id="mn-change-cover-pic" >'+
                        '<div class="overlayer-wrapper">'+
                        '<button href="" id="mn-change-cover-pic"><p><i class="fa fa-camera"></i>&nbsp;&nbsp;ប្តូររូបភាព</p></button>'+
                        '</div>'+
                        '</div>'+
                        '<img id="mn-user-cover-pic" src="' + coverUrl + '" alt="">'+
                        '</div>'+
                        '<div class="tiles white">'+
                        '<div class="row top-pro">'+
                        '<div class="col-md-3 col-sm-3">'+
                        '<div class="user-profile-pic" id="mn-user-profile-pic">'+
                        '<img width="69" height="69" src="' + profilepic + '" alt="">'+
                        '<button id="mn-change-profile-pic">' +
                        '<p><i class="fa fa-camera"></i>&nbsp;&nbsp;ប្តូររូបភាព</p>'+
                        '</button>'+
                        '</div>'+
                        '<div class="user-mini-description">'+
                        '<h3 class="semi-bold">'+ username + '</h3>'+
                        '<br>'+
                        '<h5>(២០០១-២០១៣)</h5>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-lg-8 col-md-8 col-sm-8 user-description-box">'+
                        '<p><i class="fa fa-graduation-cap"></i><b>ការសិក្សា</b> &nbsp;&nbsp;'+ school + '</p>'+
                        '<p><i class="fa fa-user"></i><b>ភេទ</b> &nbsp;&nbsp;'+ gender + '</p>'+
                        '<p><i class="fa fa-map-marker"></i><b>ទីកន្លែងកំណើត</b> &nbsp;&nbsp;'+ pob +'</p>'+
                        '<p><i class="fa fa-birthday-cake"></i><b>ថ្ងៃខែឆ្នាំកំណើត</b> &nbsp;&nbsp;'+ dob +'</p>'+
                        '<a  href="/user-gallery?userId=' + data.id +'">' +
                        '<button class="btn btn-primary btn-sm btn-small pull-right" id="mn-user-gallery">រូបភាព ' +
                        '</button>' +
                        '<a/>'+
                        '</div>' +
                        '<div class="clearfix"></div>'+
                        '</div>'+
                        '</div>'
                    );
                } else {
                    $.ajax({
                        url: "/v1/api/user/checkRemember/"+uid+"/"+mateId,
                        type: "GET",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function (data) {
                            var tagElement = '<button id="remeberedMate"  class="btn btn-primary btn-small"><i class="fa fa-check"></i>&nbsp;&nbsp;បានស្គាល់</button>';
                            myStatus = false;
                            if(data == "false")
                            {
                                tagElement = '<button id="remeberedMate" class="btn btn-primary btn-small"><i></i>&nbsp;&nbsp;ស្គាល់</button>';
                                myStatus = true;
                            } // checkIfRemember is true
                            $("#page-title").text(username);
                            $("#mn-user-timeline").append(
                                '<div class="tiles green cover-pic-wrapper">'+
                                '<div class="overlayer bottom-right">'+
                                '<div class="overlayer-wrapper">'+
                                '<div class="padding-10">'+
                                tagElement+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '<img id="mn-user-cover" src="' + coverUrl + '" alt="">'+
                                '</div>'+
                                '<div class="tiles white">'+
                                '<div class="row">'+
                                '<div class="col-md-3 col-sm-3">'+
                                '<div class="user-profile-pic" id="mn-user-profile-pic">'+
                                '<img width="69" height="69" src="' + profilepic + '" alt="">'+
                                '</div>'+
                                '<div class="user-mini-description">'+
                                '<h3 class="semi-bold">'+ username + '</h3>'+
                                '<br>'+
                                '<h5>(២០០១-២០១៣)</h5>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-lg-8 col-md-8 col-sm-8 user-description-box">'+
                                '<p><i class="fa fa-graduation-cap"></i><b>ការសិក្សា</b> &nbsp;&nbsp;'+ school + '</p>'+
                                '<p><i class="fa fa-user"></i><b>ភេទ</b> &nbsp;&nbsp;'+ gender + '</p>'+
                                '<p><i class="fa fa-map-marker"></i><b>ទីកន្លែងកំណើត</b> &nbsp;&nbsp;'+ pob +'</p>'+
                                '<p><i class="fa fa-birthday-cake"></i><b>ថ្ងៃខែឆ្នាំកំណើត</b> &nbsp;&nbsp;'+ dob +'</p>'+
                                '<a  href="/user-gallery?userId=' + mateId +'" id="mn-btn-gallery">' +
                                '<button class="btn btn-primary btn-sm btn-small pull-right" >រូបភាព ' +
                                '</button>' +
                                '<a/>'+
                                '</div>'+
                                '<div class="clearfix"></div>'+
                                '</div>'+
                                '</div>');
                        },
                        error: function (data) {

                        }
                    });//checkIfRemember
                }
            },
            error: function (err) {
                swal("មិនដំណើរការ", err.message, "error");
            }
        });
    } //findMateByMateId
    var findPostsByMateId = function (mateId, matePage) {

        if(matePage == undefined)
            matePage = 1;

        $("li#g-loading").remove();
        $("#postWall li:eq(0)").append("<li id='g-loading'>" +
            "<div style='text-align: center'>" +
            "<img style='height: 60px;width: 100px;' src='/images/spinner.gif'>" +
            "</div>" +
            "</li>"
        );

        $.ajax({
            url: "/v1/api/post/mate?mateId=" + mateId + "&page=" + matePage,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                //xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
            },
            success: function (data) {
                // console.log(data.data);
                $("li#g-loading").remove();

                if(matePage <= 1) {
                    $("#postWall li:eq(0)").nextAll().empty();
                }

                if(data.pagination.total_pages >= matePage) {
                    for (var i = 0; i < data.data.length; i++) {
                        // var d = new Date(data.data[i].createdDate);
                        // var weekday = new Array(7);
                        // weekday[0] =  "Sunday";
                        // weekday[1] = "Monday";
                        // weekday[2] = "Tuesday";
                        // weekday[3] = "Wednesday";
                        // weekday[4] = "Thursday";
                        // weekday[5] = "Friday";
                        // weekday[6] = "Saturday";
                        // var postTime = "On " + weekday[d.getDay()] + ", " + d.getDate() + " - " + d.getHours() + ":" + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
                        var postTime = timeAgo(data.data[i].createdDate);
                        if(postTime == "Yesterday") {
                            var d = new Date(data.data[i].createdDate);
                            postTime += " at " + (d.getHours() > 12 ? d.getHours() - 12 + ":" + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes() + " PM" : d.getHours() + ":" + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes() + " AM");
                        }
                        var content = data.data[i].content;
                        if(content.length > 200) {
                            content = content.substr(0, 200) + " ...";
                        }
                        if (data.data[i].photoUrl != "") {
                            if(content.length > 200) {
                                if(data.data[i].user.id == uid) {
                                    $("#postWall").append("<li>" +
                                        "<div class='cbp_tmicon animated bounceIn'>" +
                                        "<a href='/user-profile?userId='" + data.data[i].user.id + "''>" +
                                        "<img class='img-responsive user-photo' src=" + data.data[i].user.profilePhotoUrl + " >" +
                                        "</a>" +
                                        "</div>" +
                                        "<div class='cbp_tmlabel'>" +
                                        "<div class='p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5' style='word-break: break-all'>" +
                                        "<h4 class='inline m-b-5'><span class='text-success semi-bold'>" + data.data[i].user.firstName + " " + data.data[i].user.lastName + "</span> </h4>" +

                                        "<div  class='quicklinks pull-right'>"+
                                        "<div data-toggle='dropdown' class='dropdown-toggle'>" +
                                        "<i style='cursor: pointer;' class='material-icons'>keyboard_arrow_down</i>" +
                                        "</div>" +
                                        "<ul class='dropdown-menu' role='menu'>" +
                                        "<li>" +
                                        "<li style='cursor: pointer'>" +
                                        "<a data-id='" + data.data[i].id + "' id='editPost' data-toggle='modal' data-target='#mn-modal-edit-post'><i class='material-icons'>create</i>&nbsp&nbsp&nbspកែប្រែ</a>" +
                                        "</li>" +
                                        "<li class='divider'></li>" +
                                        "<li style='cursor: pointer'>" +
                                        "<a data-id='" + data.data[i].id + "' id='deletePost'><i class='material-icons'>delete_forever</i>&nbsp&nbsp&nbspលុប</a>" +
                                        "</li>" +
                                        "</li>" +
                                        "</ul>" +
                                        "</div>" +
                                        "<div class='muted'>" + postTime + "</div>" +
                                        "<p id='pContent' class='m-t-5 dark-text'>" + content + "</p>" +
                                        "<a data-id='" + data.data[i].id + "' href='javascript:void(0)' data-check='1' class='muted'>Show more</a> </div>" +
                                        "</div>" +
                                        "<div id ='photo'  data-toggle='modal' data-target='#mn-modal-popPost' class='m-l-10 m-r-10 xs-m-l-5 xs-m-r-5' style='padding-bottom: 2%; cursor: pointer;' onclick='findPostByPostId("+data.data[i].id+")'>" +
                                        "<img src=" + data.data[i].photoUrl + " style='width:100%'> " +
                                        "</div>"+
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class='tiles padding-10' style='background-color: white'>" +
                                        "       <div class='pull-left' >" +
                                        "           <a href='javascript:void(0);' onclick='likePost("+data.data[i].id+")' data-heart=\"0\"><img src=\"images/icon/heart.png\" id='mn-like-post"+data.data[i].id+"' ></a>" +
                                        "        </div>" +
                                        "        <div class=\"pull-right\">" +
                                        "            <a href=\"javascript:void(0);\" id=\"comment-post\" onclick='commentPost("+data.data[i].id+")' ><img src=\"images/icon/comment.png\" ></a>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" id='mn-like-container"+data.data[i].id+"' style =\"background-color: white; height:32px;\">" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"height: 40px; background-color: white; \">" +
                                        "       <div>" +
                                        "          <input type=\"text\" id='mn-comment"+data.data[i].id+"' placeholder=\"បញ្ចេញមតិរបស់អ្នក......\" style='position: relative; width:100% ;margin-top: -9px'  onkeydown = \"if (event.keyCode == 13)\n" +
                                        "                        document.getElementById('myBtn"+data.data[i].id+"').click()\"> "+
                                        "               <button hidden='hidden' onclick='btnClick("+data.data[i].id+")' id='myBtn"+data.data[i].id+"'>Button</button>"+
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"background-color: white;\" id='mn-comment-form'>" +
                                        "<input type='hidden' id='cmt-page"+data.data[i].id+"'/>"+
                                        "<input value='1' type='hidden' id='prev-page"+data.data[i].id+"'/>"+
                                        "<div id='previous-comment" + data.data[i].id + "' style='display: none'>"+
                                        "       <a href='javascript:void(0)' onclick='previousComment("+data.data[i].id+")'>មើលការផ្ដល់មតិពីមុន</a> " +
                                        "</div>"+
                                        "       <div class=\"pull-left\">" +
                                        "           <ul id='comment-container"+data.data[i].id+"'>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        "           </ul>"+
                                        // "          <a href=\"https://www.w3schools.com\"><img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish</a><p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    getAllCommentByPostId(data.data[i].id);
                                    countLike(data.data[i].id);
                                }
                                else {
                                    $("#postWall").append("<li>" +
                                        "<div class='cbp_tmicon animated bounceIn'>" +
                                        "<a href='/user-profile?userId='" + data.data[i].user.id + "''>" +
                                        "<img class='img-responsive user-photo' src=" + data.data[i].user.profilePhotoUrl + " >" +
                                        "</a>" +
                                        "</div>" +
                                        "<div class='cbp_tmlabel'>" +
                                        "<div class='p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5' style='word-break: break-all'>" +
                                        "<h4 class='inline m-b-5'><span class='text-success semi-bold'>" + data.data[i].user.firstName + " " + data.data[i].user.lastName + "</span> </h4>" +
                                        "<div class='muted'>" + postTime + "</div>" +
                                        "<p class='m-t-5 dark-text'>" + content + "</p>" +
                                        "<a data-id='" + data.data[i].id + "' href='javascript:void(0)' data-check='1' class='muted'>Show more</a> </div>" +
                                        "</div>" +
                                        "<div id ='photo'  data-toggle='modal' data-target='#mn-modal-popPost' class='m-l-10 m-r-10 xs-m-l-5 xs-m-r-5' style='padding-bottom: 2%; cursor: pointer;' onclick='findPostByPostId("+data.data[i].id+")'>" +
                                        "<img src=" + data.data[i].photoUrl + " style='width:100%'> " +
                                        "</div>"+
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class='tiles padding-10' style='background-color: white'>" +
                                        "       <div class='pull-left' >" +
                                        "           <a href='javascript:void(0);' onclick='likePost("+data.data[i].id+")' data-heart=\"0\"><img src=\"images/icon/heart.png\" id='mn-like-post"+data.data[i].id+"' ></a>" +
                                        "        </div>" +
                                        "        <div class=\"pull-right\">" +
                                        "            <a href=\"javascript:void(0);\" id=\"comment-post\" onclick='commentPost("+data.data[i].id+")' ><img src=\"images/icon/comment.png\" ></a>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" id='mn-like-container"+data.data[i].id+"' style =\"background-color: white; height:32px;\">" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"height: 40px; background-color: white; \">" +
                                        "       <div>" +
                                        "          <input type=\"text\" id='mn-comment"+data.data[i].id+"' placeholder=\"បញ្ចេញមតិរបស់អ្នក......\" style='position: relative; width:100% ;margin-top: -9px'  onkeydown = \"if (event.keyCode == 13)\n" +
                                        "                        document.getElementById('myBtn"+data.data[i].id+"').click()\"> "+
                                        "               <button hidden='hidden' onclick='btnClick("+data.data[i].id+")' id='myBtn"+data.data[i].id+"'>Button</button>"+
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"background-color: white;\" id='mn-comment-form'>" +
                                        "<input type='hidden' id='cmt-page"+data.data[i].id+"'/>"+
                                        "<input value='1' type='hidden' id='prev-page"+data.data[i].id+"'/>"+
                                        "<div id='previous-comment" + data.data[i].id + "' style='display: none'>"+
                                        "       <a href='javascript:void(0)' onclick='previousComment("+data.data[i].id+")'>មើលការផ្ដល់មតិពីមុន</a> " +
                                        "</div>"+
                                        "       <div class=\"pull-left\">" +
                                        "           <ul id='comment-container"+data.data[i].id+"'>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        "           </ul>"+
                                        // "          <a href=\"https://www.w3schools.com\"><img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish</a><p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    getAllCommentByPostId(data.data[i].id);
                                    countLike(data.data[i].id);
                                }
                            }
                            else {
                                if(data.data[i].user.id == uid) {
                                    $("#postWall").append("<li>" +
                                        "<div class='cbp_tmicon animated bounceIn'>" +
                                        "<a href='/user-profile?userId=" + data.data[i].user.id + "'>" +
                                        "<img class='img-responsive user-photo' src=" + data.data[i].user.profilePhotoUrl + " >" +
                                        "</a>" +
                                        "</div>" +
                                        "<div class='cbp_tmlabel'>" +
                                        "<div class='p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5' style='word-break: break-all'>" +
                                        "<h4 class='inline m-b-5'><span class='text-success semi-bold'>" + data.data[i].user.firstName + " " + data.data[i].user.lastName + "</span> </h4>" +

                                        "<div  class='quicklinks pull-right'>"+
                                        "<div data-toggle='dropdown' class='dropdown-toggle'>" +
                                        "<i style='cursor: pointer;' class='material-icons'>keyboard_arrow_down</i>" +
                                        "</div>" +
                                        "<ul class='dropdown-menu' role='menu'>" +
                                        "<li>" +
                                        "<li style='cursor: pointer'>" +
                                        "<a data-id='" + data.data[i].id + "' id='editPost' data-toggle='modal' data-target='#mn-modal-edit-post'><i class='material-icons'>create</i>&nbsp&nbsp&nbspកែប្រែ</a>" +
                                        "</li>" +
                                        "<li class='divider'></li>" +
                                        "<li style='cursor: pointer'>" +
                                        "<a data-id='" + data.data[i].id + "' id='deletePost'><i class='material-icons'>delete_forever</i>&nbsp&nbsp&nbspលុប</a>" +
                                        "</li>" +
                                        "</li>" +
                                        "</ul>" +
                                        "</div>" +

                                        "<div class='muted'>" + postTime + "</div>" +
                                        "<p id='pContent' class='m-t-5 dark-text'>" + content + "</p>" +
                                        "</div>" +
                                        "<div id ='photo'  data-toggle='modal' data-target='#mn-modal-popPost' class='m-l-10 m-r-10 xs-m-l-5 xs-m-r-5' style='padding-bottom: 2%; cursor: pointer;' onclick='findPostByPostId("+data.data[i].id+")'>" +
                                        "<img src=" + data.data[i].photoUrl + " style='width:100%'> " +
                                        "</div>"+
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class='tiles padding-10' style='background-color: white'>" +
                                        "       <div class='pull-left' >" +
                                        "           <a href='javascript:void(0);' onclick='likePost("+data.data[i].id+")' data-heart=\"0\"><img src=\"images/icon/heart.png\" id='mn-like-post"+data.data[i].id+"' ></a>" +
                                        "        </div>" +
                                        "        <div class=\"pull-right\">" +
                                        "            <a href=\"javascript:void(0);\" id=\"comment-post\" onclick='commentPost("+data.data[i].id+")' ><img src=\"images/icon/comment.png\" ></a>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" id='mn-like-container"+data.data[i].id+"' style =\"background-color: white; height:32px;\">" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"height: 40px; background-color: white; \">" +
                                        "       <div>" +
                                        "          <input type=\"text\" id='mn-comment"+data.data[i].id+"' placeholder=\"បញ្ចេញមតិរបស់អ្នក......\" style='position: relative; width:100% ;margin-top: -9px'  onkeydown = \"if (event.keyCode == 13)\n" +
                                        "                        document.getElementById('myBtn"+data.data[i].id+"').click()\"> "+
                                        "               <button hidden='hidden' onclick='btnClick("+data.data[i].id+")' id='myBtn"+data.data[i].id+"'>Button</button>"+
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"background-color: white;\" id='mn-comment-form'>" +
                                        "<input type='hidden' id='cmt-page"+data.data[i].id+"'/>"+
                                        "<input value='1' type='hidden' id='prev-page"+data.data[i].id+"'/>"+
                                        "<div id='previous-comment" + data.data[i].id + "' style='display: none'>"+
                                        "       <a href='javascript:void(0)' onclick='previousComment("+data.data[i].id+")'>មើលការផ្ដល់មតិពីមុន</a> " +
                                        "</div>"+
                                        "       <div class=\"pull-left\">" +
                                        "           <ul id='comment-container"+data.data[i].id+"'>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        "           </ul>"+
                                        // "          <a href=\"https://www.w3schools.com\"><img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish</a><p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    getAllCommentByPostId(data.data[i].id);
                                    countLike(data.data[i].id);
                                }
                                else {
                                    $("#postWall").append("<li>" +
                                        "<div class='cbp_tmicon animated bounceIn'>" +
                                        "<a href='/user-profile?userId=" + data.data[i].user.id + "'>" +
                                        "<img class='img-responsive user-photo' src=" + data.data[i].user.profilePhotoUrl + " >" +
                                        "</a>" +
                                        "</div>" +
                                        "<div class='cbp_tmlabel'>" +
                                        "<div class='p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5' style='word-break: break-all'>" +
                                        "<h4 class='inline m-b-5'><span class='text-success semi-bold'>" + data.data[i].user.firstName + " " + data.data[i].user.lastName + "</span> </h4>" +
                                        "<div class='muted'>" + postTime + "</div>" +
                                        "<p class='m-t-5 dark-text'>" + content + "</p>" +
                                        "</div>" +
                                        "<div id ='photo'  data-toggle='modal' data-target='#mn-modal-popPost' class='m-l-10 m-r-10 xs-m-l-5 xs-m-r-5' style='padding-bottom: 2%; cursor: pointer;' onclick='findPostByPostId("+data.data[i].id+")'>" +
                                        "<img src=" + data.data[i].photoUrl + " style='width:100%'> " +
                                        "</div>"+
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class='tiles padding-10' style='background-color: white'>" +
                                        "       <div class='pull-left' >" +
                                        "           <a href='javascript:void(0);' onclick='likePost("+data.data[i].id+")' data-heart=\"0\"><img src=\"images/icon/heart.png\" id='mn-like-post"+data.data[i].id+"' ></a>" +
                                        "        </div>" +
                                        "        <div class=\"pull-right\">" +
                                        "            <a href=\"javascript:void(0);\" id=\"comment-post\" onclick='commentPost("+data.data[i].id+")' ><img src=\"images/icon/comment.png\" ></a>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" id='mn-like-container"+data.data[i].id+"' style =\"background-color: white; height:32px;\">" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"height: 40px; background-color: white; \">" +
                                        "       <div>" +
                                        "          <input type=\"text\" id='mn-comment"+data.data[i].id+"' placeholder=\"បញ្ចេញមតិរបស់អ្នក......\" style='position: relative; width:100% ;margin-top: -9px'  onkeydown = \"if (event.keyCode == 13)\n" +
                                        "                        document.getElementById('myBtn"+data.data[i].id+"').click()\"> "+
                                        "               <button hidden='hidden' onclick='btnClick("+data.data[i].id+")' id='myBtn"+data.data[i].id+"'>Button</button>"+
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"background-color: white;\" id='mn-comment-form'>" +
                                        "<input type='hidden' id='cmt-page"+data.data[i].id+"'/>"+
                                        "<input value='1' type='hidden' id='prev-page"+data.data[i].id+"'/>"+
                                        "<div id='previous-comment" + data.data[i].id + "' style='display: none'>"+
                                        "       <a href='javascript:void(0)' onclick='previousComment("+data.data[i].id+")'>មើលការផ្ដល់មតិពីមុន</a> " +
                                        "</div>"+
                                        "       <div class=\"pull-left\">" +
                                        "           <ul id='comment-container"+data.data[i].id+"'>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        "           </ul>"+
                                        // "          <a href=\"https://www.w3schools.com\"><img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish</a><p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    getAllCommentByPostId(data.data[i].id);
                                    countLike(data.data[i].id);
                                }
                            }
                        }
                        else {
                            if(content.length > 200) {
                                if(data.data[i].user.id == uid) {
                                    $("#postWall").append("<li>" +
                                        "<div class='cbp_tmicon animated bounceIn'>" +
                                        "<a href='/user-profile?userId=" + data.data[i].user.id + "'>" +
                                        "<img class='img-responsive user-photo' src=" + data.data[i].user.profilePhotoUrl + " >" +
                                        "</a>" +
                                        "</div>" +
                                        "<div class='cbp_tmlabel'>" +
                                        " <div class='p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5' style='word-break: break-all'>" +
                                        "<h4 class='inline m-b-5'><span class='text-success semi-bold'>" + data.data[i].user.firstName + " " + data.data[i].user.lastName + "</span> </h4>" +

                                        "<div  class='quicklinks pull-right'>"+
                                        "<div data-toggle='dropdown' class='dropdown-toggle'>" +
                                        "<i style='cursor: pointer;' class='material-icons'>keyboard_arrow_down</i>" +
                                        "</div>" +
                                        "<ul class='dropdown-menu' role='menu'>" +
                                        "<li>" +
                                        "<li style='cursor: pointer'>" +
                                        "<a data-id='" + data.data[i].id + "' id='editPost' data-toggle='modal' data-target='#mn-modal-edit-post'><i class='material-icons'>create</i>&nbsp&nbsp&nbspកែប្រែ</a>" +
                                        "</li>" +
                                        "<li class='divider'></li>" +
                                        "<li style='cursor: pointer'>" +
                                        "<a data-id='" + data.data[i].id + "' id='deletePost'><i class='material-icons'>delete_forever</i>&nbsp&nbsp&nbspលុប</a>" +
                                        "</li>" +
                                        "</li>" +
                                        "</ul>" +
                                        "</div>" +

                                        "<div class='muted'>" + postTime + "</div>" +
                                        "<p id='pContent' data-id='" + data.data[i].id + "' class='m-t-5 dark-text'>" + content + "</p>" +
                                        "<a data-id='" + data.data[i].id + "' href='javascript:void(0)' data-check='1' class='muted'>Show more</a> </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class='tiles padding-10' style='background-color: white'>" +
                                        "       <div class='pull-left' >" +
                                        "           <a href='javascript:void(0);' onclick='likePost("+data.data[i].id+")' data-heart=\"0\"><img src=\"images/icon/heart.png\" id='mn-like-post"+data.data[i].id+"' ></a>" +
                                        "        </div>" +
                                        "        <div class=\"pull-right\">" +
                                        "            <a href=\"javascript:void(0);\" id=\"comment-post\" onclick='commentPost("+data.data[i].id+")' ><img src=\"images/icon/comment.png\" ></a>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" id='mn-like-container"+data.data[i].id+"' style =\"background-color: white; height:32px;\">" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"height: 40px; background-color: white; \">" +
                                        "       <div>" +
                                        "          <input type=\"text\" id='mn-comment"+data.data[i].id+"' placeholder=\"បញ្ចេញមតិរបស់អ្នក......\" style='position: relative; width:100% ;margin-top: -9px'  onkeydown = \"if (event.keyCode == 13)\n" +
                                        "                        document.getElementById('myBtn"+data.data[i].id+"').click()\"> "+
                                        "               <button hidden='hidden' onclick='btnClick("+data.data[i].id+")' id='myBtn"+data.data[i].id+"'>Button</button>"+
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"background-color: white;\" id='mn-comment-form'>" +
                                        "<input type='hidden' id='cmt-page"+data.data[i].id+"'/>"+
                                        "<input value='1' type='hidden' id='prev-page"+data.data[i].id+"'/>"+
                                        "<div id='previous-comment" + data.data[i].id + "' style='display: none'>"+
                                        "       <a href='javascript:void(0)' onclick='previousComment("+data.data[i].id+")'>មើលការផ្ដល់មតិពីមុន</a> " +
                                        "</div>"+
                                        "       <div class=\"pull-left\">" +
                                        "           <ul id='comment-container"+data.data[i].id+"'>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        "           </ul>"+
                                        // "          <a href=\"https://www.w3schools.com\"><img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish</a><p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    getAllCommentByPostId(data.data[i].id);
                                    countLike(data.data[i].id);
                                }
                                else {
                                    $("#postWall").append("<li>" +
                                        "<div class='cbp_tmicon animated bounceIn'>" +
                                        "<a href='/user-profile?userId=" + data.data[i].user.id + "'>" +
                                        "<img class='img-responsive user-photo' src=" + data.data[i].user.profilePhotoUrl + " >" +
                                        "</a>" +
                                        "</div>" +
                                        "<div class='cbp_tmlabel'>" +
                                        " <div class='p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5' style='word-break: break-all'>" +
                                        "<h4 class='inline m-b-5'><span class='text-success semi-bold'>" + data.data[i].user.firstName + " " + data.data[i].user.lastName + "</span> </h4>" +
                                        "<div class='muted'>" + postTime + "</div>" +
                                        "<p data-id='" + data.data[i].id + "' class='m-t-5 dark-text'>" + content + "</p>" +
                                        "<a data-id='" + data.data[i].id + "' href='javascript:void(0)' data-check='1' class='muted'>Show more</a> </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class='tiles padding-10' style='background-color: white'>" +
                                        "       <div class='pull-left' >" +
                                        "           <a href='javascript:void(0);' onclick='likePost("+data.data[i].id+")' data-heart=\"0\"><img src=\"images/icon/heart.png\" id='mn-like-post"+data.data[i].id+"' ></a>" +
                                        "        </div>" +
                                        "        <div class=\"pull-right\">" +
                                        "            <a href=\"javascript:void(0);\" id=\"comment-post\" onclick='commentPost("+data.data[i].id+")' ><img src=\"images/icon/comment.png\" ></a>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" id='mn-like-container"+data.data[i].id+"' style =\"background-color: white; height:32px;\">" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"height: 40px; background-color: white; \">" +
                                        "       <div>" +
                                        "          <input type=\"text\" id='mn-comment"+data.data[i].id+"' placeholder=\"បញ្ចេញមតិរបស់អ្នក......\" style='position: relative; width:100% ;margin-top: -9px'  onkeydown = \"if (event.keyCode == 13)\n" +
                                        "                        document.getElementById('myBtn"+data.data[i].id+"').click()\"> "+
                                        "               <button hidden='hidden' onclick='btnClick("+data.data[i].id+")' id='myBtn"+data.data[i].id+"'>Button</button>"+
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"background-color: white;\" id='mn-comment-form'>" +
                                        "<input type='hidden' id='cmt-page"+data.data[i].id+"'/>"+
                                        "<input value='1' type='hidden' id='prev-page"+data.data[i].id+"'/>"+
                                        "<div id='previous-comment" + data.data[i].id + "' style='display: none'>"+
                                        "       <a href='javascript:void(0)' onclick='previousComment("+data.data[i].id+")'>មើលការផ្ដល់មតិពីមុន</a> " +
                                        "</div>"+
                                        "       <div class=\"pull-left\">" +
                                        "           <ul id='comment-container"+data.data[i].id+"'>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        "           </ul>"+
                                        // "          <a href=\"https://www.w3schools.com\"><img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish</a><p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    getAllCommentByPostId(data.data[i].id);
                                    countLike(data.data[i].id);
                                }
                            }
                            else {

                                if(data.data[i].user.id == uid) {
                                    $("#postWall").append("<li>" +
                                        "<div class='cbp_tmicon animated bounceIn'>" +
                                        "<a href='/user-profile?userId=" + data.data[i].user.id + "'>" +
                                        "<img class='img-responsive user-photo' src=" + data.data[i].user.profilePhotoUrl + " >" +
                                        "</a>" +
                                        "</div>" +
                                        "<div class='cbp_tmlabel'>" +
                                        " <div class='p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5' style='word-break: break-all'>" +
                                        "<h4 class='inline m-b-5'><span class='text-success semi-bold'>" + data.data[i].user.firstName + " " + data.data[i].user.lastName + "</span> </h4>" +

                                        "<div  class='quicklinks pull-right'>"+
                                        "<div data-toggle='dropdown' class='dropdown-toggle'>" +
                                        "<i style='cursor: pointer;' class='material-icons'>keyboard_arrow_down</i>" +
                                        "</div>" +
                                        "<ul class='dropdown-menu' role='menu'>" +
                                        "<li>" +
                                        "<li style='cursor: pointer'>" +
                                        "<a data-id='" + data.data[i].id + "' id='editPost' data-toggle='modal' data-target='#mn-modal-edit-post'><i class='material-icons'>create</i>&nbsp&nbsp&nbspកែប្រែ</a>" +
                                        "</li>" +
                                        "<li class='divider'></li>" +
                                        "<li style='cursor: pointer'>" +
                                        "<a data-id='" + data.data[i].id + "' id='deletePost'><i class='material-icons'>delete_forever</i>&nbsp&nbsp&nbspលុប</a>" +
                                        "</li>" +
                                        "</li>" +
                                        "</ul>" +
                                        "</div>" +

                                        "<div class='muted'>" + postTime + "</div>" +
                                        "<p id='pContent'  class='m-t-5 dark-text'>" + content + "</p>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class='tiles padding-10' style='background-color: white'>" +
                                        "       <div class='pull-left' >" +
                                        "           <a href='javascript:void(0);' onclick='likePost("+data.data[i].id+")' data-heart=\"0\"><img src=\"images/icon/heart.png\" id='mn-like-post"+data.data[i].id+"' ></a>" +
                                        "        </div>" +
                                        "        <div class=\"pull-right\">" +
                                        "            <a href=\"javascript:void(0);\" id=\"comment-post\" onclick='commentPost("+data.data[i].id+")' ><img src=\"images/icon/comment.png\" ></a>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" id='mn-like-container"+data.data[i].id+"' style =\"background-color: white; height:32px;\">" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"height: 40px; background-color: white; \">" +
                                        "       <div>" +
                                        "          <input type=\"text\" id='mn-comment"+data.data[i].id+"' placeholder=\"បញ្ចេញមតិរបស់អ្នក......\" style='position: relative; width:100% ;margin-top: -9px'  onkeydown = \"if (event.keyCode == 13)\n" +
                                        "                        document.getElementById('myBtn"+data.data[i].id+"').click()\"> "+
                                        "               <button hidden='hidden' onclick='btnClick("+data.data[i].id+")' id='myBtn"+data.data[i].id+"'>Button</button>"+
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"background-color: white;\" id='mn-comment-form'>" +
                                        "<input type='hidden' id='cmt-page"+data.data[i].id+"'/>"+
                                        "<input value='1' type='hidden' id='prev-page"+data.data[i].id+"'/>"+
                                        "<div id='previous-comment" + data.data[i].id + "' style='display: none'>"+
                                        "       <a href='javascript:void(0)' onclick='previousComment("+data.data[i].id+")'>មើលការផ្ដល់មតិពីមុន</a> " +
                                        "</div>"+
                                        "       <div class=\"pull-left\">" +
                                        "           <ul id='comment-container"+data.data[i].id+"'>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        "           </ul>"+
                                        // "          <a href=\"https://www.w3schools.com\"><img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish</a><p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    getAllCommentByPostId(data.data[i].id);
                                    countLike(data.data[i].id);
                                }
                                else {
                                    $("#postWall").append("<li>" +
                                        "<div class='cbp_tmicon animated bounceIn'>" +
                                        "<a href='/user-profile?userId=" + data.data[i].user.id + "'>" +
                                        "<img class='img-responsive user-photo' src=" + data.data[i].user.profilePhotoUrl + " >" +
                                        "</a>" +
                                        "</div>" +
                                        "<div class='cbp_tmlabel'>" +
                                        " <div class='p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5' style='word-break: break-all'>" +
                                        "<h4 class='inline m-b-5'><span class='text-success semi-bold'>" + data.data[i].user.firstName + " " + data.data[i].user.lastName + "</span> </h4>" +
                                        "<div class='muted'>" + postTime + "</div>" +
                                        "<p  class='m-t-5 dark-text'>" + content + "</p>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class='tiles padding-10' style='background-color: white'>" +
                                        "       <div class='pull-left' >" +
                                        "           <a href='javascript:void(0);' onclick='likePost("+data.data[i].id+")' data-heart=\"0\"><img src=\"images/icon/heart.png\" id='mn-like-post"+data.data[i].id+"' ></a>" +
                                        "        </div>" +
                                        "        <div class=\"pull-right\">" +
                                        "            <a href=\"javascript:void(0);\" id=\"comment-post\" onclick='commentPost("+data.data[i].id+")' ><img src=\"images/icon/comment.png\" ></a>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" id='mn-like-container"+data.data[i].id+"' style =\"background-color: white; height:32px;\">" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"height: 40px; background-color: white; \">" +
                                        "       <div>" +
                                        "          <input type=\"text\" id='mn-comment"+data.data[i].id+"' placeholder=\"បញ្ចេញមតិរបស់អ្នក......\" style='position: relative; width:100% ;margin-top: -9px'  onkeydown = \"if (event.keyCode == 13)\n" +
                                        "                        document.getElementById('myBtn"+data.data[i].id+"').click()\"> "+
                                        "               <button hidden='hidden' onclick='btnClick("+data.data[i].id+")' id='myBtn"+data.data[i].id+"'>Button</button>"+
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        // "<hr style='margin-top: 0;margin-bottom: 0'/>"+
                                        "    <div class=\"tiles grey padding-10\" style=\"background-color: white;\" id='mn-comment-form'>" +
                                        "<input type='hidden' id='cmt-page"+data.data[i].id+"'/>"+
                                        "<input value='1' type='hidden' id='prev-page"+data.data[i].id+"'/>"+
                                        "<div id='previous-comment" + data.data[i].id + "' style='display: none'>"+
                                        "       <a href='javascript:void(0)' onclick='previousComment("+data.data[i].id+")'>មើលការផ្ដល់មតិពីមុន</a> " +
                                        "</div>"+
                                        "       <div class=\"pull-left\">" +
                                        "           <ul id='comment-container"+data.data[i].id+"'>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        // "                <li>"+
                                        // "                   <a href=\"https://www.w3schools.com\">" +
                                        // "                       <img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish" +
                                        // "                   </a>" +
                                        // "                   <p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        // "                </li>"+
                                        "           </ul>"+
                                        // "          <a href=\"https://www.w3schools.com\"><img src=\"images/icon/sreylish.png\" style='width: 25px'>&nbsp;&nbsp;Lun Sreylish</a><p  style =\"color: black;\">&nbsp;&nbsp;Hello</p>" +
                                        "        </div>" +
                                        "        <div class=\"clearfix\"></div>" +
                                        "    </div>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    getAllCommentByPostId(data.data[i].id);
                                    countLike(data.data[i].id);
                                }
                            }
                        }
                    }
                }
                else {
                    canScroll = false;
                    page--;
                    matePage--;
                }
            },
            error: function (err) {
                swal("មិនដំណើរការ", err.message, "error");
            }
        });
    } // findPostsByMateId
    //Add Friend Request
    var saveSendFriendRequest=function (receiverId) {
        //alert("SenderID :"+uid +"  "+"ReceiverID : "+mateId);
        $.ajax({
            url:"/v1/api/friendRequest/"+receiverId,
            type:"POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
            },
            success:function (data) {
                 //console.log(data.data);
                //alert('Success Add Friend');
            }
        });
    }
    //Confirm Friend
    var confirmRequest=function (senderId) {
        //alert("SenderID :"+uid +"  "+"ReceiverID : "+mateId);
        $.ajax({
            url:"/v1/api/friendRequest/"+senderId,
            type:"POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
            },
            success:function (data) {
                //var receiveDate = new Date(data.receiveDate);
                console.log(data.data);
                alert('Success Add Friend')
            }
        });
    }
    function validateImagePost(file) {
        // var ext = file.name.split(".");
        // ext = ext[ext.length-1].toLowerCase();
        // var arrayExtensions = ["jpg" , "jpeg", "png", "bmp", "gif"];
        //
        var isValidSize = false;
        var imageSize= file.size/1000;
        if(imageSize <= 5000) {
            isValidSize = true;
        }

        // if (arrayExtensions.lastIndexOf(ext) == -1 || isValidSize == false) {
        //     swal("", "រូបភាពដែលអ្នកបានជ្រើសរើសមិនត្រឺមត្រូវទេ!", "error")
        //     return false;
        // }
        if (isValidSize == false) {
            swal("", "រូបភាពដែលបានជ្រើសរើសលើសទំហំ​ អនុញ្ញាតត្រឹមតែ៥MB ប៉ុណ្ណោះ", "error")
            return false;
        }
        return true;
    }
    $(document).on("click", "#mPost", function () {
        //alert(croppedImage)
        checkSession();
        page = "1";
        canScroll = true;
        $("#mpost").attr("disabled", "disabled");
        var postImage = $("#mn-input-image-post").prop('files')[0];
        var isValidImage;
        if(croppedImage) {
            isValidImage = validateImagePost(croppedImage);
        }
        var postWallContent = $("#postWallContent").val();

        postWallContent = postWallContent.replace(/<\/?[^>]+(>|$)/g, "");

        var formData = new FormData();
        formData.append("file", croppedImage);

        if(postWallContent.trim() == "" && croppedImage == "") {
            $("#all-post-container").append(
                "<div>"+
                "<span class='pull-left' style='color: red;font-size: 14px;font-weight: bold;padding-left: 10px;'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>សូមបញ្ចូលអត្ថបទឬ រូបភាពដើម្បីបញ្ចូន&nbsp;&nbsp;&nbsp;&nbsp;</span>"+
                "<a class='fileuploader-action fileuploader-action-remove' title='Remove' id='err-remove'><i class='fa fa-times' aria-hidden='true'></i></a>"+
                "</div>"
            );
        }

        if(croppedImage != undefined && isValidImage==true) {
            $.ajax({
                url: "/v1/api/upload",
                type: "POST",
                data: formData,
                processData:false,
                contentType:false,
                cache:false,
                beforeSend: function (xhr) {
                    //xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                },
                success: function (data) {
                    imgUrl = "/resources/images/" + data.filename;
                    var postData = {
                        "content": postWallContent,
                        "photoUrl": imgUrl
                    }
                    $.ajax({
                        url: "/v1/api/post/mate/" + mateId,
                        type: "POST",
                        data: JSON.stringify(postData),
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                            //xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                        },
                        success: function (data) {
                            $("#mpost").removeAttr("disabled");
                            // console.log(data);
                            $("#postWallContent").val("");
                            findPostsByMateId(mateId, page);
                            $("ul .fileuploader-items-list").empty();

//                                    $("input[name='fileuploader-list-files']").val("");
//                                    formData = null;
//                                    postImage.val('');

                            $('input#mn-input-image-post').val('').clone(true);
                        },
                        error: function (err) {
                            swal("មិនដំណើរការ", err.message, "error");
                        }
                    }); //Post content and image

                },
                error: function (err) {
                    swal("មិនដំណើរការ", err.message, "error");
                }
            }); //Post image and get image URL
        }

        if (postWallContent.trim() != "" && croppedImage == "") {
            var postData = {
                "content": postWallContent,
                "photoUrl": ""
            }

            // alert(mateId);

            $.ajax({
                url: "/v1/api/post/mate/" + mateId,
                type: "POST",
                data: JSON.stringify(postData),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                    //xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                },
                success: function (data) {
                    $("#mpost").removeAttr("disabled");
                    // console.log(data);
                    $("#postWallContent").val("");
                    findPostsByMateId(mateId, page);
                },
                error: function (err) {
                    swal("មិនដំណើរការ", err.message, "error");
                }
            }); //Post only content
        }
        croppedImage="";
    });
    $(document).on('click',"#err-remove",function () {
        $("#err-remove").parent("div").remove();
    })
    $(document).on('click',"#mn-btn-image",function () {
        $("#err-remove").parent("div").remove();
    })
    $(document).on('keyup',"#postWallContent",function () {
        $("#err-remove").parent("div").remove();
    })
    //Change Profile
    $(document).on("click", "#btnUpdateProfile", function () {

        checkSession();

        var userProfile = $("#mn-file-user-profile-pic").prop("files")[0];
        var isValidImage = validateImage(userProfile);

        // alert(userProfile);

        if(isValidImage) {
            var formData = new FormData();
            formData.append("file", userProfile);

            $.ajax({
                url: "/v1/api/upload",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                },
                success: function (data) {
                    var file = data.filename;
                    var photoUrl = {
                        "profilePhotoUrl": "/resources/images/" + file,
                        "coverPhotoUrl": $("img#mn-user-cover-pic").attr("src")
                    }
                    $.ajax({
                        url: "/v1/api/user",
                        type: "PUT",
                        data: JSON.stringify(photoUrl),
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                            //xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                        },
                        success: function (data) {
                            $("div#mn-user-profile-pic img").attr("src", "/resources/images/" + file);
                            $("img#user-profile").attr("src", "/resources/images/" + file);
                            $("img#mn-profile-pic").attr("src", "/resources/images/" + file);
                            // findPostsByMateId(mateId);
                        }
                    }); // Update profile photo

                },
                error: function (err) {
                    swal("មិនដំណើរការ", err.message, "error");
                }
            }); // Upload new photo
        }

    });

    //Change Cover
    $(document).on("click", "#btnUpdateCover", function () {

        checkSession();

        var userCover = $("input#mn-file-user-cover-pic").prop("files")[0];
        var isValidImage = validateImage(userCover);

        if(isValidImage) {

            var formData = new FormData();
            formData.append("file", userCover);

            $.ajax({
                url: "/v1/api/upload",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                },
                success: function (data) {

                    var file = data.filename;
                    // console.log(data);

                    // alert($("div#mn-user-profile-pic img").attr("src"));

                    var photoUrl = {
                        "profilePhotoUrl": $("div#mn-user-profile-pic img").attr("src"),
                        "coverPhotoUrl": "/resources/images/" + file
                    }

                    $.ajax({
                        url: "/v1/api/user",
                        type: "PUT",
                        data: JSON.stringify(photoUrl),
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                            // xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
                        },
                        success: function (data) {
                            $("img#mn-user-cover-pic").attr("src", "/resources/images/" + file);
                            // $("img#user-cover").attr("src", "/resources/images/" + file);
                            // $("img#mn-profile-pic").attr("src", "/resources/images/" + file);
                            // findPostsByMateId(mateId);
                        }
                    }); // Update profile photo

                },
                error: function (err) {
                    swal("មិនដំណើរការ", err.message, "error");
                }
            }); // Upload new photo
        }

    });
    
    $(document).on("scroll", function () {
        var v = 0;

        if ( (parseInt(($(window).scrollTop() + $(window).height() + v)) == parseInt($(document).height())) ||
            (parseInt(($(window).scrollTop() + $(window).height() + 1)) == parseInt($(document).height()))
        ) {
            if(canScroll) {
                page++;
                findPostsByMateId(mateId, page);
            }
        }//ScrollBottomReach
    });
    $(document).on("click", "#mn-group li:eq(1), #mn-group li:eq(2), #mn-group li:eq(3)", function () {
        window.location.replace("/")
    });
    $(document).on("click","#mn-show-more",function () {
        userGallery(mateId,pageGall);
        if(pageGall==totals)
            $("#mn-show-more").hide();
        pageGall++;
    })//user gallery

    var userGallery = function (userId,page) {
        $.ajax({
            url: "/v1/api/post/findPost?userId=" + userId+"&page="+page,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Authorization", "Basic UkVTVGZ1bFVzZXI6UkVTVGZ1bFBhc3N3b3Jk");
            },
            success: function (data) {
                totals=data.pagination.total_pages;
                for(var i = 0; i<data.data.length;i++){
                    // if(data.data[i].photoUrl!=""){
                        $("#mn-user-gallerys").append(
                            " <div class='col-md-4 col-sm-4 col-xs-4 batas' style='margin-top: -10px; margin-right: -20px'>" +
                                " <div id ='photo'  data-toggle='modal' data-target='#mn-modal-popPost' class='m-l-10 m-r-10 xs-m-l-5 xs-m-r-5' style='padding-bottom: 2%; cursor: pointer;' onclick='findPostByPostId("+data.data[i].id+")'>" +
                                " <img src='"+data.data[i].photoUrl+"'  data-lity class='img-thumbnail'/>" +
                                " </div>"+
                            " </div>"
                        )
                    // }
                }
                if(totals==1)
                    $("#mn-show-more").hide();
            },
            error: function (err) {

            }
        })
    }// user gallery
    userGallery(mateId,1);
    findMateByMateId(mateId);
    findPostsByMateId(mateId, page);
    //fileupload
    // window.addEventListener('DOMContentLoaded', function () {
    //     var avatar = document.getElementById('avatar');
    //     var image = document.getElementById('image');
    //     var input = document.getElementById('mn-input-image-post');
    //     var $alert = $('.alert');
    //     var $modal = $('#modal');
    //     var cropper;
    //     $('[data-toggle="tooltip"]').tooltip();
    //     input.addEventListener('change', function (e) {
    //         var files = e.target.files;
    //
    //         //uploade
    //         // var avatar = e.target.avatar;
    //         var done = function (url) {
    //             input.value = '';
    //             image.src = url;
    //             $alert.hide();
    //             $modal.modal('show');
    //         };
    //         var reader;
    //         var file;
    //         var url;
    //         if (files && files.length > 0) {
    //             file = files[0];
    //             if (URL) {
    //                 done(URL.createObjectURL(file));
    //             } else if (FileReader) {
    //                 reader = new FileReader();
    //                 reader.onload = function (e) {
    //                     done(reader.result);
    //                 };
    //                 reader.readAsDataURL(file);
    //             }
    //         }
    //     });
    //     $modal.on('shown.bs.modal', function () {
    //         cropper = new Cropper(image, {
    //             aspectRatio: 1,
    //             viewMode: 3,
    //         });
    //     }).on('hidden.bs.modal', function () {
    //         cropper.destroy();
    //         cropper = null;
    //     });
    //     document.getElementById('crop').addEventListener('click', function () {
    //         var initialAvatarURL;
    //         var canvas;
    //         $modal.modal('hide');
    //         if (cropper) {
    //             canvas = cropper.getCroppedCanvas({
    //                 width: 750,
    //                 height: 750,
    //             });
    //             initialAvatarURL = avatar.src;
    //             avatar.src = canvas.toDataURL();
    //             $alert.removeClass('alert-success alert-warning');
    //             canvas.toBlob(function (blob) {
    //                 croppedImage = blob;
    //             });
    //         }
    //     });
    // });
});