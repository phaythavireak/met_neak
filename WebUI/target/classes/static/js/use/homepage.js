$(document).ready(function () {

        $(".navbar a, footer a[href='#myPage']").on('click', function (event) {
            if (this.hash !== "") {
                event.preventDefault();
                // Store hash
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function () {

                    window.location.hash = hash;
                });
            }
        });

        $(".nav .navbar-nav .navbar-right .service").on('click', function (event) {
            if (this.hash !== "") {
                event.preventDefault();
                // Store hash
                var hash = this.hash;
                $('#service-block').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function () {

                    window.location.hash = hash;
                });
            }
        });

        $(".nav .navbar-nav .navbar-right .about").on('click', function (event) {
            if (this.hash !== "") {
                event.preventDefault();
                // Store hash
                var hash = this.hash;
                $('#about-block').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function () {

                    window.location.hash = hash;
                });
            }
        });



    });