$(function () {
    // upload image to profile
    $('#file-profile').change(function (event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $('#userImage').attr('src', tmppath);
        //this here for update profile users

    });


    $('#file-cover').change(function (event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        // $('#cover-image').css('background-image', 'url(' + tmppath + ')');
        $('#cover-image').attr('src',tmppath);
        alert("coer" + tmppath);

        // this for update cover photo user

    });



    // $('#img-gallery #img-click').click(function () {
    // 	var x = this.document.getElementById("img-gallery").getAttribute("src").first();
    // 	alert("Image was click..");
    // });



    //friend suggest with user-id
    $('.add-friend').click(function () {
        var v = $(this).attr('user-id');
        alert("Hello Plus" + v);
    });



    // //click image pop up
    // $('.main-box-img > a').click(function () {
    // 	var v = $(this).attr('user-img');
    // 	$('.pop-up-img').attr('src', v);
    // })


    // pop up


});


// modal image gallery
$(document).ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
        $('#show-previous-image, #show-next-image').show();
        if (counter_max == counter_current) {
            $('#show-next-image').hide();
        } else if (counter_current == 1) {
            $('#show-previous-image').hide();
        }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
        var current_image,
            selector,
            counter = 0;

        $('#show-next-image, #show-previous-image').click(function () {
            if ($(this).attr('id') == 'show-previous-image') {
                current_image--;
            } else {
                current_image++;
            }

            selector = $('[data-image-id="' + current_image + '"]');
            updateGallery(selector);
        });

        function updateGallery(selector) {
            var $sel = selector;
            current_image = $sel.data('image-id');
            $('#image-gallery-caption').text($sel.data('caption'));
            $('#image-gallery-title').text($sel.data('title'));
            $('#image-gallery-image').attr('src', $sel.data('image'));
            disableButtons(counter, $sel.data('image-id'));
        }

        if (setIDs == true) {
            $('[data-image-id]').each(function () {
                counter++;
                $(this).attr('data-image-id', counter);
            });
        }
        $(setClickAttr).on('click', function () {
            updateGallery($(this));
        });
    }






    

    // Start upload preview image
    var $uploadCrop,
        tempFilename,
        rawImg,
        imageId;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.upload-demo').addClass('ready');
                $('#cropImagePop').modal('show');
                rawImg = e.target.result;
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 1080,
            height: 350,
        },
        enforceBoundary: false,
        enableExif: true
    });
    $('#cropImagePop').on('shown.bs.modal', function () {
        // alert('Shown pop');
        $uploadCrop.croppie('bind', {
            url: rawImg
        }).then(function () {
            console.log('jQuery bind complete');
            console.log(tempFilename);

        });
    });

    $('.item-img').on('change', function () {
        imageId = $(this).data('id'); tempFilename = $(this).val();
        $('#cancelCropBtn').data('id', imageId); readFile(this);
    });

    $('#cropImageBtn').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'base64',
            format: 'jpeg',
            size: { width: 1080, height: 350 }
        }).then(function (resp) {
            $('#item-img-output').attr('src', resp);
            $('#cropImagePop').modal('hide');
            $('#images').attr('src', resp);
            console.log("was click!" + rawImg);

        });
    });

    
});



// update cover photo

